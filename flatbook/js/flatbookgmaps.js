function Flatbook(props) {
    var that = this;
    //   this.props = props;

    this.search = (function() {

        // instantiate search UI
        var performSearchTimeline = 0,
            segment1, segment2, segment3;
        var ranOnce = false;

        // other ui plugins
        var l = Ladda.create(document.querySelector(props.search));


        // ui event bindings

        // SEARCH EVENT - search button click, enter keypress
        props.$search.on('click', onInitialSearchClick);
        props.$search2.on('click', onSubsequentSearchClick);
        props.$keywords.keyup(function(e) { onInitialSearchKeyPress(e); });
        props.$keywords2.keyup(function(e) { onSubsequentSearchKeyPress(e); });

        props.$keywords.on('mouseup', focusKeywords);
        props.$keywords2.on('mouseup', focusKeywords);

        props.$drawerbutton.on('click', toggleDrawer);

        // SEARCH EVENT - events to attach to
        function focusKeywords() {
            $(this).select();
        }

        var animateSearch1 = function() {
            var performSearchTimeline = new TimelineLite();

            // segment 1
            // 1. hide the search box, the bg image and the cities list
            // 2. show the map
            segment1 = new TimelineLite({
                delay: 1
            });
            segment1.add([new TweenLite(props.omni, 0.25, {
                autoAlpha: '0',
                display: 'none'
            }), new TweenLite(props.bg2, 0.25, {
                autoAlpha: '0',
                display: 'none'
            }), new TweenLite(props.citiesFooter, 0.25, {
                autoAlpha: '0',
                display: 'none'
            })])
                .add(new TweenLite(props.gmap, 0.25, {
                    autoAlpha: '1'
                }));
            performSearchTimeline.add(segment1);

            // segment 2
            // 1. show the second search box
            // 2. (OC) show the map results
            segment2 = new TimelineLite();
            segment2.add([new TweenLite(props.omni2, 0.35, {
                autoAlpha: '1'
            }), new TweenLite([props.holder2], 0.50, { backgroundColor: 'rgba(0,0,0, 0.35)'})]);
            // , new TweenLite(props.filters2, 0.35, {
            //     autoAlpha: '1'
            // })]);
            performSearchTimeline.add(segment2);

            // segment 3
            // 1. show left filters for 3 seconds
            // 2. hide left filters
            var segment3 = new TimelineLite({
                tweens: [
                    TweenLite.from(props.filtersbutton, 0.5, {right: '-170px', ease: Back.easeOut}),
                    new TweenLite(props.filtersbutton, 0.75, {background: 'rgba(29, 114, 75, 0.75)'}),
                    new TimelineLite({
                        align: 'sequence',
                        tweens: [
                            new TweenLite(props.filtersbutton, 0.5, {autoAlpha: '1', display: 'block'})
                            //new TweenLite(props.$mapbutton, 0.75, {background: 'rgba(29, 114, 75, 0)'})
                        ]
                    })
                ]
            });
                            // new TimelineLite({
            //     onStart: showFilters,
            //     onComplete: hideFilters,
            //     tweens: [new TweenLite(props.filters, 2, {
            //         autoAlpha: '1'
            //     })]
            // });
            // segment3.eventCallback('onComplete', showMapResults);


            // // segment 3 - better
            //  1. show under searchbox filters
            //  segment3 = new TweenLite(props.body, 0, {
            //      delay: 2,
            //      onComplete: showFilters
            //  });
            performSearchTimeline.add(segment3);
            
            performSearchTimeline.play(0, false);
        };

        

        function onInitialSearchClick() {
            if (that.cities.isValid(props.$keywords.val())) {
                l.start();

                //chain(that.googlemaps.showMapResults, animateSearch1).play(0, false);
                var closer = that.googlemaps.showMapResults;
                var go = new TimelineLite({
                    onStart: closer,
                    tweens: [ new TweenLite.from(props.nothing, 0.01, {autoAlpha: "1"}) ],
                    onComplete: animateSearch1
                });

                go.play(0, false);
                // search(onStart, onEnd
                // search(that.googlemaps.showMapResults, animate() );
                // google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function() {
                //     search(initSearch, that.googlemaps.showMapResults);
                // });
                // that.googlemaps.center();
            }
        }
        function onInitialSearchKeyPress(e) {
            if (e.keyCode == 13) {
                if (that.cities.isValid(props.$keywords.val())) {
                    l.start();
                    props.$keywords.val(props.$keywords.val());
                    google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function() {
                        search(initSearch, that.googlemaps.showMapResults);
                    });
                    // that.googlemaps.center();
                }
            }
        }

        function onSubsequentSearchClick() {
            if (that.cities.isValid(props.$keywords2.val())) {

                google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function() {
                    that.googlemaps.showMapResults();
                    that.listings.hideListings();
                });
                // that.googlemaps.center();
            }
        }

        function onSubsequentSearchKeyPress(e) {
            if (e.keyCode == 13) {
                if (that.cities.isValid(props.$keywords2.val())) {


                    props.$keywords.val(props.$keywords2.val());
                    google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function() {
                        that.googlemaps.showMapResults();
                        that.listings.hideListings();
                    });
                    // that.googlemaps.center();
                }
            }
        }

        // FILTERS EVENT - search box focus, filter button click
        props.$filtersHide.on('click', hideFilters);
        props.$filtersHide.on('click', function() {
            props.$omni2.removeClass('active');
        });
        props.$filtersbutton.on('click', toggleFilters);

        // search business logic
        // ----------------------

        // setup search

        var onStart = function() {
            props.$filtersShow.removeClass('hidden');
            props.$filters.removeClass('hidden');
            props.$omni2.removeClass('hidden');

            // copy keywords to main app
            if (props.$keywords2.val() === '') {
                props.$keywords2.val(props.$keywords.val());

            }
        };

        //var onEnd = f
        // animate interaction



        function chain(onBegin, onEnd) {
            return new TimelineLite({
                onStart: onBegin,
                tweens: [ new TweenLite.from(props.nothing, 0, {color: "#000"}) ],
                onComplete: onEnd
            });
        }

        
        this._togglefilters = new TimelineLite({
            tweens: [
                new TweenLite([props.filters, props.gmap, props.menu, props.filtersbutton], 0.25, {
                    left: '+=-275px'
                }),
                //TweenLite.from(props.filtersHide, 0, { autoAlpha: '1', display: 'none'}),
                new TweenLite(props.filtersShow, 0, {
                    autoAlpha: '0',
                    display: 'none'
                })
            ]
            // tweens: [new TweenLite(props.filters2, 0.25, {
            //     autoAlpha: '0',
            //     top: '+=-30px'
            //     // }), new TweenLite(props.filters2, 0.5, {
            //     //     top: '+=-150px'
            //     // })]
            // })]
        }).pause();

        var filtersShown = false;
        var filtersIsAnim = false;
        $icon = props.$filtersbutton.find('.glyphicon');
        $text = props.$filtersbutton.find('.button-text');

        function hideFilters() {
            if (filtersShown) {
                _togglefilters.reverse(0);
                filtersShown = false;
                $icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                $text.html('show filters');
            }
        }

        function showFilters() {
            if (!filtersShown) {
                _togglefilters.play(0, false);
                filtersShown = true;
                $icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                $text.html('hide filters');
            }
        }

        function toggleFilters() {
            if (filtersShown) {
                hideFilters();
            } else {
                showFilters();
            }

        }


        this._toggledrawer = new TimelineLite({
            tweens: [
                TweenLite.to(props.drawer, 0.25, {
                    autoAlpha: '1',
                    display: 'inline-block'
                }),
                TweenLite.from(props.drawer, 0.25, { marginLeft: "-=150px"})
            ]
        }).pause();

        var drawerShown = false;
        var drawerIsAnim = false;

        function hideDrawer() {
            if (drawerShown) {
                _toggledrawer.reverse(0);
                drawerShown = false;
            }
        }

        function showDrawer() {
            if (!drawerShown) {
                _toggledrawer.play(0, false);
                drawerShown = true;
            }
        }

        function toggleDrawer() {
            if (drawerShown) {
                hideDrawer();
            } else {
                showDrawer();
            }

        }

        return {
            animate: function() {
                search(that.setup, that.googlemaps.showMapResults);
            },
            showFilters: showFilters,
            hideFilters: hideFilters
        };
    }());


    this.cities = (function() {

        // CITIES EVENTS - cities button click
        props.$citiesBtn.on('click', toggleCities);


        // cities animation
        var citiesTimeLine = new TimelineLite({
            tweens: [

                new TweenLite(props.citiesShow, 0.001, {
                    autoAlpha: '0',
                    display: 'none'
                }),
                new TweenLite(props.citiesHide, 0.001, {
                    autoAlpha: '1'
                }),
                new TweenLite(props.citiesFooter, 0.25, {
                    top: '40%',
                    delay: 0.001,
                    ease: Linear.easeOut
                }),
                new TweenLite(props.omni, 0.25, {
                    delay: 0.001,
                    autoAlpha: 0
                })
            ]
        }).pause();

        // cities toggle
        var citiesShown = false;

        function hideCities() {
            if (citiesShown) {
                citiesTimeLine.reverse(0);
                citiesShown = false;
            }
        }

        function showCities() {
            if (!citiesShown) {
                citiesTimeLine.play(0, false);
                citiesShown = true;
            }
        }

        function toggleCities() {
            if (citiesShown) {
                hideCities();
            } else {
                showCities();
            }

        }

        // business logic
        // ----------------------

        var cityList;
        function listCities() {
            if (typeof something === "undefined") {
                return {
                    items: ["Montreal, QC, Canada", "Toronto, ON, Canada", "Quebec City, QC, Canada", "Vancouver, BC, Canada", "Victoria, BC, Canada", "Ottawa, ON, Canada", "Calgary, AB, Canada"],
                    coords: [
                        [45.508670, -73.553992],
                        [43.65323, -79.38318],
                        [46.80328, -71.24280],
                        [49.26123, -123.11393],
                        [48.42842, -123.36564],
                        [45.42153, -75.69719],
                        [51.04532, -114.05810]
                    ],
                    values: [1, 2, 3, 4, 5, 6, 7]
                };
            } else {
                return cityList;
            }

        }
        function typeaheadList() {
            return listCities().items;
        }

        function listCurrentCoords() {
            var lat = listCities().coords[listCities().items.indexOf(value)][0];
            var lng = listCities().coords[listCities().items.indexOf(value)][1];


            var value = props.$keywords.val();
            if (value !== '')
                return {
                    lat: lat,
                    lng: lng
                };
            else  // montreal
                return {
                    lat: 45.508670,
                    lng: -73.553992
                };
        }

        function isValid(value) {
            var result = listCities().items.indexOf(value) != -1;

            if (result) {
                props.$error.addClass('hidden');
                props.$error2.addClass('hidden');
            } else {
                props.$error.removeClass('hidden');
                props.$error2.removeClass('hidden');
            }

            return result;
        }

        // return
        return {
            listCities: function() {
                return (listCities());
            },
            listCurrentCoords: listCurrentCoords,
            isValid: isValid,
            hideCities: hideCities,
            showCities: showCities,
            toggleCities: toggleCities,
            typeaheadList: typeaheadList
        };
    }());

    this.googlemaps = (function() {

        // /* maps */
        // var infowindow, map;
        // props.map = map;
        // var gray = [{
        //     "stylers": [{
        //         "hue": "#ff1a00"
        //     }, {
        //         "invert_lightness": true
        //     }, {
        //         "saturation": -100
        //     }, {
        //         "lightness": 33
        //     }, {
        //         "gamma": 0.5
        //     }]
        // }, {
        //     "featureType": "water",
        //     "elementType": "geometry",
        //     "stylers": [{
        //         "color": "#2D333C"
        //     }]
        // }];


        // // center map by default
        // var centerdef = new google.maps.LatLng(45.508670, -73.553992);
        // var mapOptions = {
        //     zoom: 14,
        //     center: centerdef,
        //     styles: gray,
        //     mapTypeControl: false,
        //     panControl: false,
        //     zoomControl: false,
        //     scaleControl: false,
        //     streetViewControl: false,
        //     scrollwheel: false,
        //     draggable: false
        // };

       var gmap;
    
        gmap = new GMaps({
            el: '#map-canvas',
            lat: -12.043333,
            lng: -77.028333,
            zoomControl : true,
            zoomControlOpt: {
                style : 'SMALL',
                position: 'TOP_LEFT'
            },
            panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false
        });
    
        

        map = gmap.map; //new google.maps.Map(document.getElementById(props.gmap.substring(1)), mapOptions);

        infowindow = new google.maps.InfoWindow({
          content: '',
          anchorPoint: new google.maps.Point(0, 0)
        });

        var _showMarker = new TimelineLite({
            tweens: [
                new TweenLite(props.$card, 0.5, {autoAlpha: '1', display: 'block'})
            ]
        }).pause();
        // var _slideMarker = new TimelineLite({
        //     tweens: [
        //         TweenLite.from(props.$cardin, 0.5, {x: "-=200"})
        //     ]
        // }).pause();
        var markerShown = false;

        function hideMarker() {
            if (markerShown) {
                _showMarker.reverse(0);
                markerShown = false;
            }
        }

        function showMarker(cb) {
            if (!markerShown) {
                cb();
                _showMarker.play(0, false);

            } else {
                cb();
            }

            markerShown = true;
        }

        function toggleMarker() {
            if (markerShown) {
                hideMarker();
            } else {
                showMarker();
            }

        }

        function createMarker(place, index) {
            var marker = new google.maps.Marker({
                map: map,
                position: place,
                animation: google.maps.Animation.DROP
            });

            //open onclick
            google.maps.event.addListener(marker, 'click', function() {
                setMarker(this);
                //infowindow.setContent(Mustache.to_html(props.$infowindowTpl.html(), searchResults.listings[index]));

                showMarker(function() {
                    props.$card.html(Mustache.to_html(props.$infowindowTpl.html(), searchResults.listings[index]));
                    props.$card.show();
                    $(props.closecard).on('click', function() {
                        props.$card.hide();
                        console.log('closing card');
                    });
                });

                //infowindow.open(map, this);
            });
        }

        // display map results

        var markers = [], searchResults, selectedMarker;
        function showMapResults(lat, lng) {


           
            // center to current search
            //center();

            // clear old markers
            // if (markers && markers.length !== 0) {
            //     for (var i = 0; i < markers.length; ++i) {
            //         markers[i].setMap(null);
            //     }

            //     markers.length = 0;
            // }

            // wow this is 472 times better than that was
            gmap.removeMarkers();

            // perform search and put them on the map
            searchResults = that.listings.search();

            var onclick = function(e) {
                if (console.log)
                    console.log(e);
                    alert('You clicked in this marker');
            };
            var mouseover = function(e) {
                if (console.log)
                    console.log(e);
            };

            for (var j = 0; j < searchResults.listings.length; j++) {
                var listing = searchResults.listings[j];

                 gmap.addMarker({
                    lat: listing.coords[0],
                    lng: listing.coords[1],
                    title: 'Lima',
                    details: {
                      database_id: 42,
                      author: 'HPNeo'
                    },
                    click: onclick,
                    mouseover: mouseover
                });

            }

           

            // var newmapOptions = {
            //     zoomControl: true,
            //     scaleControl: true,
            //     scrollwheel: true,
            //     draggable: true
            //  };

            // map.setOptions(newmapOptions);
            that.search.hideFilters();
            props.$numresults.html(searchResults.listings.length);

            var coords = that.cities.listCurrentCoords;

            gmap.setCenter(coords.lat, coords.lng);

            //showResultsTimeline.play(0, false);
        }

        function setMarker(marker) {
            if (typeof selectedMarker !== 'undefined')
                selectedMarker.setIcon("http://www.google.com/mapfiles/marker.png");
            marker.setIcon("http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png");

            infowindow.open(map, marker);

            selectedMarker = marker;
        }

        //google.maps.event.addDomListener(window, 'load', initialize);
        var listingsTemplate = props.$listingsTpl.html();

        var showResultsTimeline = (function() {
            var t = new TimelineLite({
                tweens: [
                    TweenLite.from(props.$resultsbutton, 2, {bottom: '-100px', ease: Elastic.easeOut}),
                    new TweenLite(props.$resultsbutton, 0.75, {background: 'rgba(29, 114, 75, 0.75)'}),
                    new TimelineLite({
                        align: 'sequence',
                        tweens: [
                            new TweenLite(props.$resultsbutton, 0.5, {autoAlpha: '1', display: 'block'})
                            //new TweenLite(props.$resultsbutton, 0.75, {delay: 0.5, background: 'rgba(29, 114, 75, 0)'})
                        ]
                    })
                ],
                onStart: function() {
                    props.$listingsResults.html(Mustache.to_html(listingsTemplate, searchResults));
                }
            }).pause();
            return t;
        }());

        function center() {
            var latLng = that.cities.listCurrentCoords();
            gmap.setCenter(latLng[0], latLng[1]);
        }

        google.maps.event.addListenerOnce(map, 'idle', function() {
            // do something only the first time the map is loaded
            var startingTimeline = new TimelineLite();
            startingTimeline.add(new TweenLite(props.body, 0.75, {
                autoAlpha: '1'
            }))
            // .add(new TweenLite(props.city, 0.5, {
            //     delay: 0.25,
            //     autoAlpha: '1'
            // }))
            .add([new TweenLite(props.bg, 0.75, {
                delay: 0.75,
                autoAlpha: '0',
                display: 'none'
            }), new TweenLite(props.bg2, 0.75, {
                delay: 0.75,
                autoAlpha: '1'
            }), new TweenLite(props.city, 0.65, {
                delay: 0.75,
                autoAlpha: '0'
            })], [new TweenLite(props.searchbox, 0.5, {
                delay: 0.5,
                autoAlpha: '1'
            }), new TweenLite(props.slogan, 0.5, {
                delay: 0.5,
                autoAlpha: '1'
            })]);

            startingTimeline.play(0, false);


        });

        return {
            map: map,
            showMapResults: showMapResults,
            center: center
        };
    }());

    this.menu = (function() {

        // MENU EVENTS - menu button click
        props.$menu.on('click', toggleMenu);

        // menu animation
        var menuTimeLine = new TimelineLite({
            align: 'sequence',
            tweens: [
                new TweenLite([props.omni2], 0.05, {
                    autoAlpha: '0', display: 'none'
                }),
                new TimelineLite({
                    tweens: [
                        new TweenLite(props.logo, 0.25, {
                            autoAlpha: '0'
                        }),
                        new TweenLite(props.navbar, 0.25, {
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }),
                        new TweenLite(props.nav, 0.25, {
                            autoAlpha: '1', display: 'block'
                        })
                    ]
                })
            ]
        }).pause();

        // menu toggle
        var menuShown = false;

        function hideMenu() {
            if (menuShown) {
                menuTimeLine.reverse(0);
                menuShown = false;
            }
        }

        function showMenu() {
            if (!menuShown) {
                menuTimeLine.play(0, false);
                menuShown = true;
            }
        }

        function toggleMenu() {
            if (menuShown) {
                hideMenu();
            } else {
                showMenu();
            }

        }

        return {
            hideMenu: hideMenu,
            showMenu: showMenu,
            toggleMenu: toggleMenu
        };
    }());

    this.listings = (function() {

        var showMapButtonTimeline = (function() {
            var t = new TimelineLite({
                tweens: [
                    TweenLite.from(props.$mapbutton, 0.25, {top: '-50px', ease: Back.easeOut}),
                    new TweenLite(props.$mapbutton, 0.75, {background: 'rgba(29, 114, 75, 0.75)'}),
                    new TweenLite(props.$mapbutton, 0.25, {autoAlpha: '1', display: 'block'})
                ]
            });
            return t;
        }());

        var listingsTimeline = new TimelineLite({
            align: 'sequence',
            tweens: [
                new TweenLite([props.listings], 0, { autoAlpha: '1', display: 'block'}),
                new TweenLite([props.listings], 0.20, { top: '-146px', ease: Linear.easeOut }),
                new TimelineLite({
                    tweens: [
                        new TweenLite(props.gmap, 0.20, { autoAlpha: '0', display: 'none'}),
                        new TweenLite([props.navbar], 0.01, { backgroundColor: '#000000'}),
                        new TweenLite([props.holder2], 0.01, { backgroundColor: '#252528'})
                    ]
                }),
                showMapButtonTimeline
            ]
        }).pause();


        // cities toggle
        var listingsShown = false;
        var $icon = props.$resultsbutton.find('.glyphicon');
        function hideListings() {
            if (listingsShown) {
                listingsTimeline.reverse(-0.5);
                listingsShown = false;
                $icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
        }

        function showListings() {
            if (!listingsShown) {
                listingsTimeline.play(0, false);
                listingsShown = true;
                $icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        }

        function toggleListings() {
            if (listingsShown) {
                hideListings();
            } else {
                showListings();
            }

        }

        // ui event bindings
        props.$resultsbutton.on('click', toggleListings);
        props.$mapbutton.on('click', toggleListings);


        function search(filters) {
            if (that.cities.listCities().items.indexOf(props.$keywords.val()) == 5) {
                var listings = {
                    listings: [
                        {
                            title: 'The Plateau Experience',
                            shortDescription: 'Situated right next to the Parc Mont-Royal, The Plateau Experience is the ideal place for a quiet getaway while still be close to the action',
                            price: '$250',
                            imageUrls: ['https://www.flatbook.ca/dimages/cache/3/2a9a5fb8-5841-49ec-93b3-694c08bb3203.JPG'],
                            description: '',
                            available: '',
                            amenities: '',
                            beds: '',
                            baths: '',
                            rules: '',
                            reviews: '',
                            coords: [45.41846, -75.69788]
                        },
                        {
                            title: 'Two Story, 2BR Plateau Apartment',
                            shortDescription: 'Located right beside the famous Mont-Royal Avenue, you will find many little cafés, restaurants, boutiques and a truly charming ambiance.',
                            price: '$250',
                            imageUrls: ['https://www.flatbook.ca/dimages/cache/3/2a9a5fb8-5841-49ec-93b3-694c08bb3203.JPG'],
                            description: '',
                            available: '',
                            amenities: '',
                            beds: '',
                            baths: '',
                            rules: '',
                            reviews: '',
                            coords: [45.41273, -75.68655]
                        },
                        {
                            title: 'Beautiful 1BR in the Plateau',
                            shortDescription: 'Located right beside the famous Mont-Royal Avenue, you will find many little cafés, restaurants, boutiques and a truly charming ambiance.',
                            price: '$250',
                            imageUrls: ['https://www.flatbook.ca/dimages/cache/3/2a9a5fb8-5841-49ec-93b3-694c08bb3203.JPG'],
                            description: '',
                            available: '',
                            amenities: '',
                            beds: '',
                            baths: '',
                            rules: '',
                            reviews: '',
                            coords: [45.40532, -75.69865]
                        },
                        {
                            title: 'The Plateau Experience',
                            shortDescription: 'Situated right next to the Parc Mont-Royal, The Plateau Experience is the ideal place for a quiet getaway while still be close to the action',
                            price: '$250',
                            imageUrls: ['https://www.flatbook.ca/dimages/cache/3/2a9a5fb8-5841-49ec-93b3-694c08bb3203.JPG'],
                            description: '',
                            available: '',
                            amenities: '',
                            beds: '',
                            baths: '',
                            rules: '',
                            reviews: '',
                            coords: [45.40394, -75.69530]
                        }
                    ]
                };

                return listings;
            }

            return { listings: [] };
        }

        return {
            search: search,
            showListings: showListings,
            hideListings: hideListings,
            toggleListings: toggleListings
        };
    }());

    // event binding after object is built
    // typeahead plugin
    var typeaheadOptions = {
            matcher: function(item) {
                if (item.toLowerCase().indexOf(this.query.toLowerCase()) !== -1) return true;
                return false;
            },
            showHintOnFocus: true,
            source: that.cities.typeaheadList,
            autoselect: true,
            updater: function(item) {
                console.log('updating!: ' + arguments[0]);
                if (that.cities.isValid(item)) {
                    props.$error.addClass('hidden');
                }
                else {
                    props.$error.removeClass('hidden');
                }
                return item;
            }
        };
    props.$keywords.typeahead(typeaheadOptions);
    props.$keywords2.typeahead(typeaheadOptions);

    // date plugin
    var dateOptions = {
        pickDate: true,
        pickTime: false,
        startDate: 1 / 1 / 1970,
        useStrict: false
    };

    props.$from.datetimepicker(dateOptions);
    props.$to.datetimepicker(dateOptions);
}
