var flatbook = new Flatbook({
    navbar: '#fb-navbar', $navbar: $('#fb-navbar'),
    nav: '#fb-nav', $nav: $('#fb-nav'),
    logo: '#fb-logo', $logo: $('#fb-logo'),
    menu: '#fb-menu', $menu: $('#fb-menu'),
    citiesFooter: '#fb-citiesFooter', $citiesFooter: $('#fb-citiesFooter'),
    citiesShow: '#fb-citiesShow', $citiesShow: $('#fb-citiesShow'),
    citiesHide: '#fb-citiesHide', $citiesHide: $('#fb-citiesHide'),
    omni: '#fb-omni', $omni: $('#fb-omni'),
    searchbox: '#fb-search', $searchbox: $('#fb-search'),
    slogan: '#fb-slogan', $slogan: $('#fb-slogan'),
    city: '#fb-city', $city: $('#fb-city'),
    omni2: '#fb-omni2', $omni2: $('#fb-omni2'),
    holder2: '#fb-holder2', $holder2: $('#fb-holder2'),
    error: '#fb-error', $error: $('#fb-error'),
    error2: '#fb-error', $error2: $('#fb-error2'),
    citiesBtn: '#fb-citiesBtn', $citiesBtn: $('#fb-citiesBtn'),
    listings: '#fb-listings', $listings: $('#fb-listings'),
    bg: '#fb-bg', $bg: $('#fb-bg'),
    bg2: '#fb-bg2', $bg2: $('#fb-bg2'),
    gmap: '#map-canvas', $gmap: $('#map-canvas'),
    filters: '#fb-filters', $filters: $('#fb-filters'),
    // filters2: '#fb-filters2', $filters2: $('#fb-filters2'),
    // filtersbutton: '#fb-filtersbutton', $filtersbutton: $('#fb-filtersbutton'),
    search: '#fb-search-btn1', $search: $('#fb-search-btn1'),
    search2: '#fb-search-btn2', $search2: $('#fb-search-btn2'),
    keywords: '#fb-keywords', $keywords: $('#fb-keywords'),
    keywords2: '#fb-keywords2', $keywords2: $('#fb-keywords2'),
    body: '#body', $body: $('#body'),
    numresults: '#fb-numresults', $numresults: $('#fb-numresults'),
    resultsbutton: '#fb-resultsbutton', $resultsbutton: $('#fb-resultsbutton'),
    mapbutton: '#fb-mapbutton', $mapbutton: $('#fb-mapbutton'),
    listingbutton: '#fb-listingbutton', $listingbutton: $('#fb-listingbutton'),
    listingsResults: '#fb-listings-results', $listingsResults: $('#fb-listings-results'),
    drawer: '#fb-drawer', $drawer: $('#fb-drawer'),
    drawerbutton: '#fb-drawer-button', $drawerbutton: $('#fb-drawer-button'),
    from: '#fb-from', $from: $('#fb-from'),
    to: '#fb-to', $to: $('#fb-to'),
    listingsTpl: '#fb-listings-tpl', $listingsTpl: $('#fb-listings-tpl'),
    listingTpl: '#fb-listing-tpl', $listingTpl: $('#fb-listing-tpl'),
    listingResultTpl: '#fb-listing-result-tpl', $listingResultTpl: $('#fb-listing-result-tpl'),
    infowindowTpl: '#fb-infowindow-tpl', $infowindowTpl: $('#fb-infowindow-tpl'),
    infowindow: '#fb-infowindow', $infowindow: $('#fb-infowindow'),
    cardin: '#fb-card-in', $cardin: $('#fb-card-in'),
    closecard: '#fb-close-card', $closecard: $('#fb-close-card'),
    nothing: '#fb-nothing', $nothing: $('#fb-nothing'),
    filterbutton: '#fb-applyFilters', $filterbutton: $('#fb-applyFilters'),
    filterCheckin: '#fb-ft-checkin', $filterCheckin: $('#fb-ft-checkin'),
    filterCheckout: '#fb-ft-checkout', $filterCheckout: $('#fb-ft-checkout'),
    filterGuests: '#fb-ft-guests', $filterGuests: $('#fb-ft-guests'),
    filterBeds: '#fb-ft-beds', $filterBeds: $('#fb-ft-beds'),
    filterBaths: '#fb-ft-baths', $filterBaths: $('#fb-ft-baths'),
    overlay: '#fb-overlay', $overlay: $('#fb-overlay'),
    listing: '#fb-listing', $listing: $('#fb-listing'),
    listtypebutton: '#fb-list-type', $listtypebutton: $('#fb-list-type'),
    markers: '.overlay', $markers: $('.overlay'),
    selectedListing: '#fb-selected-listing', $selectedListing: $('#fb-selected-listing'),
    listingResult: '#fb-listing-result', $listingResult: $('#fb-listing-result')
    
});





/* ui to backend interaction */

var $kw2 = $('#fb-omni2 input'),
    $kw2icon = $('#fb-omni2 .btn span');
$kw2.on('focus', function() {
    $kw2icon.removeClass('glyphicon-pencil').addClass('glyphicon-search');
});
$kw2.on('blur', function() {
    $kw2icon.removeClass('glyphicon-search').addClass('glyphicon-pencil');
});
