  /* deps: 
          - tween.js 
          - 
          */

          
          function Orchestration(config) {
  // evil 
  var that = this;

  // state vars
  var menuShown = false;
  var citiesShown = false;
  var filtersShown = false;


  // ui elements
  var el = config.elements; 

  // --------------------
  // animation functions
  // --------------------

  // -- toggle navbar/menu
  var menuTimeLine;
  
  this.toggleMenu = function(segments) {
    var delay = 0.25;

    // on first click
    // hide the logo, show the menu, set the opacity to 0.5
    if (!menuTimeLine) {
      menuTimeLine = new TimelineLite();
      menuTimeLine.add([
        new TweenLite(el.logo, 0.25, {autoAlpha:'0'}), 
        new TweenLite(el.navbar, 0.25, {backgroundColor: "rgba(0,0,0,0.5)"}), 
        new TweenLite(el.nav, 0.25, {autoAlpha:1})]);
    }

    if (menuShown) {
      menuTimeLine.reverse();
    }
    else
    {
      menuTimeLine.play();
    }

    menuShown = !menuShown;
  }

  // -- toggle cities list
  var citiesTimeLine;

  this.toggleCities = function(segments) {
    var delay = 0.25;
    var outTime = 250;

    // hide the 'show' button, show the 'hide' button
    // slide up the cities list, fade out the search box
    if (!citiesTimeLine) {
      citiesTimeLine = new TimelineLite();
      citiesTimeLine.add([new TweenLite(el.citiesShow, 0.001, {autoAlpha: '0', display: 'none' }), new TweenLite(el.citiesHide, 0.001, {autoAlpha: '1' })])
      .add([new TweenLite(el.citiesFooter, 0.25, {top: '40%',  ease:Linear.easeOut }), new TweenLite(el.omni, 0.25, {autoAlpha: 0})]);
    }

    if (this.citiesShown) {
      citiesTimeLine.reverse();
    }
    else
    {
      citiesTimeLine.play(0);
    }

    this.citiesShown = !this.citiesShown;
  }

  // -- search orchestration
  this.search = function(segments) {
    mainTimeline = new TimelineLite({ onStart: segments.setup });

    // segment 1
    // 1. hide the search box, the bg image and the cities list
    // 2. show the map
    var segment1 = new TimelineLite();
    segment1.add([ new TweenLite(el.omni, .25, {autoAlpha: '0', display: 'none'}), new TweenLite(el.bg, .25, {autoAlpha: '0', display: 'none'}), new TweenLite(el.citiesFooter, 0.25, {autoAlpha: '0', display: 'none'})])
    .add(new TweenLite(el.map, .25, {autoAlpha: '1'}));

    // segment 2
    // 1. show the second search box
    // 2. (OC) show the map results
    var segment2 = new TimelineLite({ onComplete: segments.showMapResults});
    segment2.add(new TweenLite(el.omni2, 0.35, {autoAlpha: '1'}))

    // segment 3
    // 1. show left filters for 3 seconds
    // 2. hide left filters
    // var segment3 = new TimelineLite({ onStart: function() {
    //   that.toggleFilters(segments);
    // }});
    // segment3.add(new TimelineLite({delay: 2, onComplete: function() {
    //   that.toggleFilters(segments);
    // }}));

    // segment 3 - better
    // 1. show under searchbox filters 
    var segment3 = new TimelineLite();
    
    segment3.add(new TweenLite(el.filters2, 0.25, {delay: 1, height: '45px', paddingTop: '5px', paddingBottom: '5px'}));

    // sequence and run
    mainTimeline.add(segment1).add(segment2).add(segment3);
    mainTimeline.play(0);
  }

  // -- toggle filters
  var showFiltersTimeline;
  this.toggleFilters = function(segments) {
    if (!showFiltersTimeline) {
      showFiltersTimeline = new TimelineLite();
      showFiltersTimeline.add([
      new TweenLite(el.filtersShow, 0, {autoAlpha: '0', display: 'none'}),      // hide 'show' button
      new TweenLite(el.filtersHide, 0, {autoAlpha: '1', display: 'block'})])    // show 'hide' button    
      .add([
        new TweenLite(el.filters, .5, {right: '-70px', ease: Back.easeOut}), // slide filters from the right
        new TweenLite(el.menu, .5, {right: "+=275px", ease: Back.easeOut})]).pause();            // shift menu button over
    }

    if (filtersShown){
      showFiltersTimeline.reverse();
    }
    else {
      showFiltersTimeline.play(0);
    }

    filtersShown = !filtersShown;
  }
  // since events are being attached in a for loop, i need to do this http://stackoverflow.com/questions/4091765/assign-click-handlers-in-for-loop
  var createHandler = function(exec, segments) {

    return function() {
      //alert('hey');
      exec(segments)
    }
  }
  var createEnterHandler = function(e, exec, segments) {
      if (e.keyCode == 13) {
        exec(segments);
      }
  }
  // wire events
  for (var key in config.targets) {
    var target = config.targets[key];
    // attach event to handler - this will map the 'handler' event to the 'attachTo' target and run the 'orchestration' function passing the 'elements'. 
    // more normal looking example: $('attachTo').click(function() { orchestration(); });
    for (var i=0; i < target.attachTo.length; i++)  {
      if (target.attachTo[i].handler == 'fb-enter') {
        target.attachTo[i].elem.keypress(function (e) {
          createEnterHandler(e, that[target.orchestration], (target.sequence ? target.sequence.segments : null))
        });
     }
     else {
      target.attachTo[i].elem.on(target.attachTo[i].handler, createHandler(that[target.orchestration], (target.sequence ? target.sequence.segments : null)));
    }
  }
    // yeah, wtf?
  }
  
  
}



