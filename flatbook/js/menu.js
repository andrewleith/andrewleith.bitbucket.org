

function Menu(props) {
    var that = this;

    this.menu = (function() {

        // MENU EVENTS - menu button click
        props.$menu.on('click', toggleMenu);

        // menu animation
        var menuTimeLine = new TimelineLite({
            align: 'sequence',
            tweens: [
                new TimelineLite({
                    tweens: [
                        new TweenLite(props.logo, 0.25, {
                            autoAlpha: '0'
                        }),
                        new TweenLite(props.navbar, 0.25, {
                            backgroundColor: 'rgba(47,145,107,0.3)'
                        }),
                        new TweenLite(props.nav, 0.25, {
                            autoAlpha: '1', display: 'block'
                        })
                    ]
                })
            ],
        }).pause();

        // menu toggle
        var menuShown = false;

        function hideMenu() {
            if (menuShown) {
                menuTimeLine.reverse(0);
                menuShown = false;
            }
        }

        function showMenu() {
            if (!menuShown) {
                menuTimeLine.play(0, false);
                menuShown = true;
            }
        }

        function toggleMenu() {
            if (menuShown) {
                hideMenu();
            } else {
                showMenu();
            }

        }

        return {
            hideMenu: hideMenu,
            showMenu: showMenu,
            toggleMenu: toggleMenu
        };
    }());

}

var menu = new Menu({
    navbar: '#fb-navbar', $navbar: $('#fb-navbar'),
    nav: '#fb-nav', $nav: $('#fb-nav'),
    logo: '#fb-logo', $logo: $('#fb-logo'),
    menu: '#fb-menu', $menu: $('#fb-menu'),
    
});