function Flatbook(props) {
    var that = this;
    var filterOptions;
    //   this.props = props;

    var Animations = {
        ShowSplashScreen:  new TimelineLite({
            tweens: [
                new TweenLite(props.body, 0.75, { autoAlpha: '1' }),
                new TimelineLite({
                    tweens: [
                        // new TweenLite(props.bg, 0.75, { delay: 0.75, autoAlpha: '0', display: 'none' }),
                        // new TweenLite(props.bg2, 0.75, { delay: 0.75, autoAlpha: '1' }),
                        new TweenLite(props.city, 0.65, { delay: 0.75, autoAlpha: '0' })
                    ]
                }),
                new TimelineLite({
                    tweens: [
                        new TweenLite(props.searchbox, 0.5, { delay: 0.5, autoAlpha: '1' }),
                        new TweenLite(props.slogan, 0.5, { delay: 0.5, autoAlpha: '1' })
                    ]
                })
            ]
        }).pause(),

        HideSplashScreen: new TimelineLite({
            tweens: [
                new TweenLite(props.omni, 0.25, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.bg, 0.25, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.citiesFooter, 0.25, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.gmap, 0.25, { delay: 0.25, autoAlpha: '1' }),
                new TweenLite(props.omni2, 0.35, { delay: 0.5, autoAlpha: '1' })
            ]
        }).pause(),

        ShowCities: new TimelineLite({
            tweens: [
                new TweenLite(props.citiesShow, 0.001, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.citiesHide, 0.001, { autoAlpha: '1' }),
                new TweenLite(props.citiesFooter, 0.25, { top: '40%', delay: 0.001, ease: Linear.easeOut }),
                new TweenLite(props.omni, 0.25, { delay: 0.001, autoAlpha: 0 })
            ]
        }).pause(),

        ShowMenu: new TimelineLite({
            align: 'sequence',
            tweens: [
            new TweenLite([props.omni2], 0.05, {
                autoAlpha: '0', display: 'none'
            }),
            new TimelineLite({
                tweens: [
                new TweenLite(props.logo, 0.25, {
                    autoAlpha: '0'
                }),
                new TweenLite(props.navbar, 0.25, {
                    backgroundColor: 'rgba(47,145,107,0.3)'
                }),
                new TweenLite(props.nav, 0.25, {
                    autoAlpha: '1', display: 'block'
                })
                ]
            })
            ]
        }).pause(),

        ShowFilters: new TimelineLite({
            tweens: [
                TweenLite.from([props.filters], 0.25, { y: '+=-50', ease: Back.easeOut }),
                TweenLite.to([props.filters], 0.25, { autoAlpha: '1', display: 'inline-block' })
            ]
        }).pause(),

        ShowMapResults: new TimelineLite({
            // onComplete: function () {
            //     if (searchResults.listings.length === 0)
            //         props.$selectedListing.hide();
            // },
            tweens: [
                TweenLite.from(props.$resultsbutton, 2, { y: '100%', ease: Elastic.easeOut }),
                new TweenLite(props.$resultsbutton, 0.75, { background: 'rgba(29, 114, 75, 0.75)' }),
                new TweenLite(props.$resultsbutton, 0.5, { autoAlpha: '1', display: 'block' })
            ]
        }).pause(),

        ShowListings: new TimelineLite({
            onStart: function () {
                props.$listingsResults.isotope({
                    layoutMode: 'masonry',
                    itemSelector: '.fb-card',
                    masonry: {
                        columnWidth: 20,
                        gutterWidth: 10
                    }
                });
            },
            tweens: [
                new TweenLite(props.listings, 0.5, { opacity: '1' }),
                new TweenLite(props.listings, 0.001, { zIndex: '2' }),
                new TweenLite(props.selectedListing, 0.25, { autoAlpha: '0' }),
                new TweenLite(props.selectedListing, 0.001, { delay: 0.25, zIndex: -1 }),
                new TweenLite(props.navbar, 0.5, {backgroundColor: 'rgba(0, 0, 0, 0.5)' }),
                TweenLite.from(props.mapbutton, 0.25, { delay: 0.5, y: '-50px', ease: Back.easeOut, immediateRender: false }),
                new TweenLite(props.mapbutton, 0.25, { delay: 0.5, autoAlpha: '1', display: 'block' })
            ]
        }).pause(),

        ShowListing: new TimelineLite({
            onComplete: function () {
                // run scrollspy now to fix position: relative issues
                $('[data-spy="scroll"]').each(function () {
                    var $spy = $(this).scrollspy('refresh');
                });
            },
            onReverseComplete: function() {
                props.$listingbutton.find('a').html('<span class="glyphicon glyphicon-list"></span> back to listings</a>');
            },
            tweens: [
                new TweenLite(props.listing, 0.001, { display: 'block' }),
                new TweenLite(props.listing, 0.15, { autoAlpha: '1' }),
                new TweenLite(props.listingbutton, 0.0001, { display: 'block' }),
                TweenLite.from(props.listingbutton, 0.25, { delay: 0.5, y: '-50px', ease: Back.easeOut }),
                new TweenLite(props.listingbutton, 0.25, { delay: 0.5, autoAlpha: '1' }),
                new TweenLite(props.navbar, 0.25, { backgroundColor: 'rgba(0, 0, 0, 0.5)'})
            ]
        }).pause()

    };

    this.search = (function () {

        // instantiate search UI
        var performSearchTimeline = 0,
        segment1, segment2, segment3;
        var ranOnce = false;

        // other ui plugins
        var l = Ladda.create(document.querySelector(props.search));


        // ui event bindings

        // SEARCH EVENT - search button click, enter keypress
        props.$search.on('click', onInitialSearchClick);
        props.$search2.on('click', onSubsequentSearchClick);
        props.$keywords.keyup(function (e) { onInitialSearchKeyPress(e); });
        props.$keywords2.keyup(function (e) { onSubsequentSearchKeyPress(e); });

        props.$keywords.on('mouseup', focusKeywords);
        props.$keywords2.on('mouseup', focusKeywords);
        props.$drawerbutton.on('click', function () {
            toggleFilters();
        });

        // SEARCH EVENT - events to attach to
        function focusKeywords() {
            $(this).select();
        }
        function onInitialSearchClick() {
            initApp();
        }
        function onInitialSearchKeyPress(e) {
            if (e.keyCode == 13) {
                initApp();
            }
        }
        function initApp() {
            if (that.cities.isValid(props.$keywords.val())) {
                l.start();
                props.$keywords.val(props.$keywords.val());
                google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function () {
                    ShowApp(that.googlemaps.showMapResults);
                });
                that.googlemaps.center();
            }
        }
        function onSubsequentSearchClick() {
            if (that.cities.isValid(props.$keywords2.val())) {
                google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function () {
                    that.googlemaps.showMapResults();
                    that.listings.hideListings();
                });
                that.googlemaps.center();
            }
        }

        function onSubsequentSearchKeyPress(e) {
            if (e.keyCode == 13) {
                Pushed = true;
                History.pushState({ route: 'search' }, "Search3", "?search33333");

                if (that.cities.isValid(props.$keywords2.val())) {
                    props.$keywords.val(props.$keywords2.val());
                    google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function () {
                        that.googlemaps.showMapResults();
                        that.listings.hideListings();
                    });
                    that.googlemaps.center();
                }
            }
        }


        //props.$filtersbutton.on('click', toggleFilters);

        // search business logic
        // ----------------------
        // animate interaction
        function ShowApp(showMapResults) {
            props.$omni2.removeClass('hiddenfb');

            // copy keywords to main app
            if (props.$keywords2.val() === '') {
                props.$keywords2.val(props.$keywords.val());

            }

            // when the animation finishes, remove the images from the dom and show the mapresults
            Animations.HideSplashScreen.eventCallback('onComplete', function() {
                props.$bg.remove();
                props.$bg2.remove();
                showMapResults();
            });

            Animations.HideSplashScreen.play(-2, false);
            
        }

        

        var filtersShown = false;

        function hideFilters() {
            if (filtersShown) {
                Animations.ShowFilters.reverse(0);
                filtersShown = false;
                props.$drawerbutton.removeClass('active');
            }
        }

        function showFilters() {
            if (!filtersShown) {
                Animations.ShowFilters.play(0);
                filtersShown = true;
                props.$drawerbutton.addClass('active');
            }
        }

        function toggleFilters() {
            if (filtersShown) {
                hideFilters();
            } else {
                showFilters();
            }

        }


        return {
            animate: function () {
                search(that.setup, that.googlemaps.showMapResults);
            }
        };
    } ());


    this.cities = (function () {

        // CITIES EVENTS - cities button click
        props.$citiesBtn.on('click', toggleCities);



        // cities toggle
        var citiesShown = false;

        function hideCities() {
            if (citiesShown) {
                Animations.ShowCities.reverse(0);
                citiesShown = false;
            }
        }

        function showCities() {
            if (!citiesShown) {
                Animations.ShowCities.play(0);
                citiesShown = true;
            }
        }

        function toggleCities() {
            if (citiesShown) {
                hideCities();
            } else {
                showCities();
            }

        }

        // business logic
        // ----------------------

        var cityList;
        function listCities() {
            if (typeof something === "undefined") {
                return {
                    items: ["Montreal, QC, Canada", "Toronto, ON, Canada", "Quebec City, QC, Canada", "Vancouver, BC, Canada", "Victoria, BC, Canada", "Ottawa, ON, Canada", "Calgary, AB, Canada"],
                    coords: [
                    [45.508670, -73.553992],
                    [43.65323, -79.38318],
                    [46.80328, -71.24280],
                    [49.26123, -123.11393],
                    [48.42842, -123.36564],
                    [45.42153, -75.69719],
                    [51.04532, -114.05810]
                    ],
                    values: [1, 2, 3, 4, 5, 6, 7]
                };
            } else {
                return cityList;
            }

        }
        function typeaheadList() {
            return listCities().items;
        }

        function listCurrentCoords() {
            var value = props.$keywords.val();
            if (value !== '')
                return listCities().coords[listCities().items.indexOf(value)];
            else
                return [0, 0];
        }

        function isValid(value) {
            var result = listCities().items.indexOf(value) != -1;

            if (result) {
                props.$error.addClass('hiddenfb');
                props.$error2.addClass('hiddenfb');
            } else {
                props.$error.removeClass('hiddenfb');
                props.$error2.removeClass('hiddenfb');
            }

            return result;
        }

        // return
        return {
            listCities: function () {
                return (listCities());
            },
            listCurrentCoords: listCurrentCoords,
            isValid: isValid,
            hideCities: hideCities,
            showCities: showCities,
            toggleCities: toggleCities,
            typeaheadList: typeaheadList
        };
    } ());

    this.googlemaps = (function () {

        /* maps */
        var infowindow, map;
        props.map = map;
        var gray = [{
            "stylers": [{
                "hue": "#ff1a00"
            }, {
                "invert_lightness": true
            }, {
                "saturation": -100
            }, {
                "lightness": 33
            }, {
                "gamma": 0.5
            }]
        }, {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{
                "color": "#2D333C"
            }]
        }];


        // center map by default
        var centerdef = new google.maps.LatLng(0, 0);
        var mapOptions = {
            draggable: true,
            zoom: 13,
            mapTypeControl: false,
            panControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: false,
            streetViewControl: false,
            scrollwheel: true
        };
        var GMAPOptions = {
            div: '#map-canvas',
            zoom: 13,
            lat: 0,
            lng: 0,
            mapTypeControl: false,
            panControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: false,
            streetViewControl: false,
            scrollwheel: true,
        };

        // GMAPS stuff
        var MapWrapper = new GMaps(GMAPOptions);
        map = MapWrapper.map;
        MapWrapper.addStyle({
            styledMapName: "Styled Map",
            styles: gray,
            mapTypeId: "map_style"
        });
        MapWrapper.setStyle("map_style");

        // display map results

        var markers = [], searchResults, ids = [];
        function showMapResults() {
            // center to current search
            center();

            props.$selectedListing.hide();

            // GMaps implementation
            MapWrapper.removeMarkers();
            $('.overlay').remove();

            // add markers and info windows for each result
            searchResults = that.listings.search();
            for (var j = 0; j < searchResults.listings.length; j++) {
                var listing = searchResults.listings[j];
                // var o = MapWrapper.drawOverlay({
                //     lat: listing.coords[0],
                //     lng: listing.coords[1],
                //     content: '<div class="overlay" id="marker-' + j + '">' + (j+1) + '<a href="#" class="btn btn-lg test">1<br /><br /></div><div class="overlay_arrow above"></div></div>'
                // });

                var m = MapWrapper.addMarker({
                    lat: listing.coords[0],
                    lng: listing.coords[1],
                    animation: google.maps.Animation.DROP,
                    //infoWindow: { content: Mustache.to_html(props.$infowindowTpl.html(), searchResults.listings[j])},
                    details: {
                        id: j.toString()
                    },
                    click: markerClick,
                    icon: (j === 0 ? 'img/marker-default.png' : 'img/marker.png')
                });

                if (j === 0) {
                    selectedMarker = m;
                    props.$selectedListing.html(Mustache.to_html(props.$listingTpl.html(), searchResults.listings[j]));
                }
            }



            // fix for dynamically loaded items into the isotope
            if (props.$listingsResults.hasClass('isotope')) {
                props.$listingsResults.isotope('destroy');
                console.log("destory");
            }


            // reset zoom, enable all map controls
            var newmapOptions = {
                draggable: true,
                zoom: 13,
                mapTypeControl: false,
                panControl: false,
                zoomControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControl: false,
                streetViewControl: false,
                scrollwheel: true
            };
            map.setOptions(newmapOptions);

            updateListings();

            //showResultsTimeline.play(0, false);

            Animations.ShowMapResults.play(0);

            // if there results, show card
            if (j > 0) {
                new TweenLite(props.selectedListing, 0.001, { display: 'block' });
                new TweenLite(props.selectedListing, 0.25, { delay: 0.75, autoAlpha: '1' });
            }
        }

        var selectedMarker;
        function markerClick() {
            if (typeof selectedMarker !== "undefined") {
                selectedMarker.setIcon('img/marker.png');
            }
            selectedMarker = this;
            this.setIcon('img/marker-default.png');

            // update infowindow thing
            props.$selectedListing.html(Mustache.to_html(props.$listingTpl.html(), searchResults.listings[this.details.id]));
            //showResultsTimeline.seek(showResultsTimeline.totalDuration());
        }

        function updateListings() {
            var searchResults = that.listings.search();
            filterMarkers(searchResults);
            hideInfoWindows();

            // update results number
            props.$numresults.html(searchResults.listings.length);

            if (that.listings.listingsShown()) {
                _updateListingsTimeline.play(0, false);
            } else {
                props.$listingsResults.html(Mustache.to_html(props.$listingsTpl.html(), searchResults));
            }
        }



        function cardClick() {
            var current = this.id.substring(this.id.indexOf('-') + 1);
            props.$listingResult.html(Mustache.to_html(props.$listingResultTpl.html(), searchResults.listings[current]));

            Animations.ShowListing.play();

        }
        function cardClickNoMap() {
            var current = this.id.substring(this.id.indexOf('-') + 1);
            props.$listingResult.html(Mustache.to_html(props.$listingResultTpl.html(), searchResults.listings[current]));

            props.$listingbutton.find('a').html('<span class="glyphicon glyphicon-map-marker"></span> back to map</a>');
            Animations.ShowListing.play(0);
            
        }
        function filterMarkers(searchResults) {
            var filterResults = searchResults.listings;
            for (var j = 0; j < MapWrapper.markers.length; j++) {
                thisid = MapWrapper.markers[j].details.id;
                if (filterResults.filter(filterMarker).length === 0)
                    MapWrapper.markers[j].setVisible(false);
                else
                    MapWrapper.markers[j].setVisible(true);
            }
        }
        var thisid;
        function filterMarker(el) {
            return el.id === thisid;
        }

        function center() {
            var latLng = that.cities.listCurrentCoords();
            map.panTo(new google.maps.LatLng(latLng[0], latLng[1]));
        }




        // EVENT BINDINGS
        // bind listing click event once
        $(document).on('click', '#fb-listings .fb-card', cardClick);
        $(document).on('click', '#fb-selected-listing .fb-card', cardClickNoMap);

        // on map load
        google.maps.event.addListenerOnce(map, 'idle', function () {
            // do something only the first time the map is loaded
            Animations.ShowSplashScreen.play(0);
        });

        // on listing close
        props.$listingbutton.on('click', function () {
            Animations.ShowListing.reverse(-0.25);
        });


        // filtering
        $(document).on('change', props.filterBeds + ',' + props.filterBaths + ',' + props.filterCheckin + ',' + props.filterCheckout, filterListings);

        // date plugin
        var dateOptions = {
            pickTime: false
            //format: 'mm-dd-yyyy'
        };

        props.$filterCheckin.datepicker(dateOptions).on('changeDate', function (e) {
            props.$filterCheckin.datepicker('hide');
            filterListings();
        });
        props.$filterCheckout.datepicker(dateOptions).on('changeDate', function (e) {
            props.$filterCheckout.datepicker('hide');
            filterListings();
        });

        function filterListings() {
            that.filterOptions = { beds: props.$filterBeds.val(), baths: props.$filterBaths.val(), availability: { start: props.$filterCheckin.val(), end: props.$filterCheckout.val()} };

            var filterInput = {
                beds: props.$filterBeds.val(),
                baths: props.$filterBaths.val(),
                availability: {
                    start: props.$filterCheckin.val(),
                    end: props.$filterCheckout.val()
                }
            };

            var value = $('#fb-listings .fb-card').filter(function (index) {
                var $this = $(this);
                var currentValues = {
                    beds: parseInt($this.data('beds'), 10),
                    baths: parseInt($this.data('baths'), 10),
                    availStart: $this.data('avail-start').split(','),
                    availEnd: $this.data('avail-end').split(',')
                    // availability: {
                    //     start: props.$filterCheckin.val(),
                    //     end: props.$filterCheckout.val()
                    // }
                };

                var avail = [];
                for (var i = 0; i < currentValues.availStart.length - 1; i++) {
                    avail.push({ start: currentValues.availStart[i], end: currentValues.availEnd[i] });
                }

                var availFilter = avail.filter(availabilityFilter).length > 0;
                // var bedFilter = (filterInput.beds === '' ? true : currentValues.beds == filterInput.beds);
                // var bathFilter = (filterInput.baths === '' ? true : currentValues.baths == filterInput.baths);

                //return bedFilter && bathFilter && availFilter;
                return availFilter;
            });

            props.$listingsResults.isotope({
                filter: value
            });

            // update map markers
            $('.fb-card.isotope-item').each(function () {
                var id = this.id.substring(this.id.indexOf('-') + 1);

                if ($(this).hasClass('isotope-hidden')) {
                    MapWrapper.markers[id].setVisible(false);
                }
                else {
                    MapWrapper.markers[id].setVisible(true);
                }
            });
        }

        function availabilityFilter(el) {
            var start = (that.filterOptions.availability.start === '' ? true : parseDate(that.filterOptions.availability.start) >= parseDate(el.start));
            var end = (that.filterOptions.availability.end === '' ? true : parseDate(that.filterOptions.availability.end) <= parseDate(el.end));
            return start && end;
        }

        function hideInfoWindows() {
            // for (var i = 0; i < that.googlemaps.MapWrapper.markers.length; i++) {
            //     MapWrapper.markers[i].infoWindow.close();
            // }
        }
        return {
            map: map,
            showMapResults: showMapResults,
            center: center,
            filterMarkers: filterMarkers,
            updateListings: updateListings,
            filterListings: filterListings,
            MapWrapper: MapWrapper,
            hideInfoWindows: hideInfoWindows
        };
    } ());

    this.menu = (function () {

        // MENU EVENTS - menu button click
        props.$menu.on('click', toggleMenu);

        // menu toggle
        var menuShown = false;

        function hideMenu() {
            if (menuShown) {
                Animations.ShowMenu.reverse(0);
                menuShown = false;
            }
        }

        function showMenu() {
            if (!menuShown) {
                Animations.ShowMenu.play(0, false);
                menuShown = true;
            }
        }

        function toggleMenu() {
            if (menuShown) {
                hideMenu();
            } else {
                showMenu();
            }

        }

        return {
            hideMenu: hideMenu,
            showMenu: showMenu,
            toggleMenu: toggleMenu
        };
    } ());

    this.listings = (function () {
        var currentResults;

        // cities toggle
        var listingsShown = false;
        var $icon = props.$resultsbutton.find('.glyphicon');
        function hideListings() {
            if (listingsShown) {
                Animations.ShowListings.reverse(0.25);
                listingsShown = false;
                $icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
        }

        function showListings(onCompleteTimeline) {
            if (!listingsShown) {
                Animations.ShowListings.play(0);
                listingsShown = true;
                $icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        }

        function toggleListings() {
            if (listingsShown) {
                hideListings();
            } else {
                showListings();
            }

        }

        // ui event bindings
        props.$resultsbutton.on('click', toggleListings);
        props.$mapbutton.on('click', toggleListings);


        function search(filters) {
            if (that.cities.listCities().items.indexOf(props.$keywords.val()) == 5) {
                var listings = {
                    listings: [
                    {
                        id: '0',
                        title: 'The Plateau Experience',
                        shortDescription: 'Situated right next to the Parc Mont-Royal, The Plateau Experience is the ideal place for a quiet getaway while still be close to the action',
                        price: '$250',
                        imageUrls: ['img/thumb1.jpg'],
                        location: "Le plateau, Montreal",
                        description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                        amenities: '',
                        beds: '2',
                        baths: '1',
                        accomodates: '5',
                        rules: '',
                        reviews: '',
                        coords: [45.41846, -75.69788],
                        available: [
                            { start: '12/26/2013', end: '01/07/2014' },
                            { start: '01/31/2014', end: '02/28/2014' }
                        ],
                        booked: [
                            { start: '01/06/2014', end: '01/30/2014' }
                        ]
                    },
                    {
                        id: '1',
                        title: 'Two Story, 2BR Plateau Apartment',
                        shortDescription: 'Located right beside the famous Mont-Royal Avenue, you will find many little cafés, restaurants, boutiques and a truly charming ambiance.',
                        price: '$250',
                        imageUrls: ['img/thumb2.jpg'],
                        location: "Le plateau, Montreal",
                        description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                        amenities: '',
                        beds: '3',
                        baths: '2',
                        accomodates: '7',
                        rules: '',
                        reviews: '',
                        coords: [45.41273, -75.68655],
                        available: [
                            { start: '12/26/2013', end: '01/05/2014' },
                            { start: '01/31/2014', end: '02/28/2014' }
                        ],
                        booked: [
                            { start: '01/06/2014', end: '01/30/2014' }
                        ]
                    },
                    {
                        id: '2',
                        title: 'Beautiful 1BR in the Plateau',
                        shortDescription: 'Located right beside the famous Mont-Royal Avenue, you will find many little cafés, restaurants, boutiques and a truly charming ambiance.',
                        longDescription: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                        price: '$250',
                        imageUrls: ['img/thumb3.jpg'],
                        location: "Le plateau, Montreal",
                        description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                        amenities: '',
                        beds: '1',
                        baths: '1',
                        accomodates: '2',
                        rules: '',
                        reviews: '',
                        coords: [45.40532, -75.69865],
                        available: [
                            { start: '12/26/2013', end: '01/05/2014' },
                            { start: '01/31/2014', end: '02/28/2014' }
                        ],
                        booked: [
                            { start: '01/06/2014', end: '01/30/2014' }
                        ]
                    },
                    {
                        id: '3',
                        title: 'The Plateau Experience',
                        shortDescription: 'Situated right next to the Parc Mont-Royal, The Plateau Experience is the ideal place for a quiet getaway while still be close to the action',
                        price: '$250',
                        imageUrls: ['img/thumb4.jpg'],
                        location: "Le plateau, Montreal",
                        description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                        amenities: '',
                        beds: '2',
                        baths: '1',
                        accomodates: '4',
                        rules: '',
                        reviews: '',
                        coords: [45.40394, -75.69530],
                        available: [
                            { start: '12/26/2013', end: '01/05/2014' },
                            { start: '01/31/2014', end: '02/28/2014' }
                        ],
                        booked: [
                            { start: '01/06/2014', end: '01/30/2014' }
                        ]
                    }
                    ]
                };

                currentResults = listings.listings;

                return { listings: listings.listings };
            }

            return { listings: [],
                nolistings: true
            };
        }

        //var filterOptions = {beds: '2', baths: '', availability: { start: '12/26/2013' } };
        that.filterOptions = { beds: '', baths: '', availability: { start: '', end: ''} };

                $('.fb-li-menu a').on('click', function () {
            $('#' + this.href.substring(this.href.indexOf('#') + 1)).ScrollTo();
            event.preventDefault();
        });

        return {
            search: search,
            showListings: showListings,
            hideListings: hideListings,
            toggleListings: toggleListings,
            listingsShown: function () { return listingsShown; },
            currentResults: function () { return currentResults; }
        };
    } ());

    // event binding after object is built
    // typeahead plugin
    var typeaheadOptions = {
        matcher: function (item) {
            if (item.toLowerCase().indexOf(this.query.toLowerCase()) !== -1) return true;
            return false;
        },
        showHintOnFocus: true,
        source: that.cities.typeaheadList,
        autoselect: true,
        updater: function (item) {
            console.log('updating!: ' + arguments[0]);
            if (that.cities.isValid(item)) {
                props.$error.addClass('hiddenfb');
            }
            else {
                props.$error.removeClass('hiddenfb');
            }
            return item;
        }
    };
    props.$keywords.typeahead(typeaheadOptions);
    props.$keywords2.typeahead(typeaheadOptions);


    // hide things
    // var _a = new TimelineLite({
    //     tweens: [TweenLite.to(props.listing, 0.001, {zIndex: '-1', autoAlpha: '0'})]
    // });
}


$.fn.eqHeights = function (options) {

    var defaults = {
        child: false,
        parentSelector: null
    };
    options = $.extend(defaults, options);

    var el = $(this);
    if (el.length > 0 && !el.data('eqHeights')) {
        $(window).bind('resize.eqHeights', function () {
            el.eqHeights();
        });
        el.data('eqHeights', true);
    }

    var elmnts;
    if (options.child && options.child.length > 0) {
        elmtns = $(options.child, this);
    } else {
        elmtns = $(this).children();
    }

    var prevTop = 0;
    var max_height = 0;
    var elements = [];
    var parentEl;
    elmtns.height('auto').each(function () {

        if (options.parentSelector && parentEl !== $(this).parents(options.parentSelector).get(0)) {
            $(elements).height(max_height);
            max_height = 0;
            prevTop = 0;
            elements = [];
            parentEl = $(this).parents(options.parentSelector).get(0);
        }

        var thisTop = this.offsetTop;

        if (prevTop > 0 && prevTop != thisTop) {
            $(elements).height(max_height);
            max_height = $(this).height();
            elements = [];
        }
        max_height = Math.max(max_height, $(this).height());

        prevTop = this.offsetTop;
        elements.push(this);
    });

    $(elements).height(max_height);
};

// parse date in format mm/dd/yyyy
function parseDate(datestr) {
    var datear = datestr.split('/');
    var date = new Date(datear[2], datear[0] - 1, datear[1], 0, 0, 0, 0);

    return date;
}

// utility function - subtract two arrays
Array.prototype.diff = function (a) {
    return this.filter(function (i) { return !(a.indexOf(i) > -1); });
};



// Bind to StateChange Event
History.Adapter.bind(window, 'statechange', function() {
    var State = History.getState();

    // if (Pushed) {
    //     Pushed = false;
    //     return;
    // }

    // returns { data: { params: params }, title: "Search": url: "?search" }
    console.log(State); 

    // or you could recall search() because that's where this state was saved
    if (State.url == "?search") {
        search(data.params);
    }
});

var Pushed = false;


