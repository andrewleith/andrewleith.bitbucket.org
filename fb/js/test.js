 $('#slideLeft2').on('click', function() {
    Animations.slide('#panel2', 'left', 'x', 1);
});
 $('#slideLeft3').on('click', function() {
    Animations.slide('#panel3', 'left', 'left', 1);
});
// set panel width and hide panels to start
TweenLite.to($('.panel'), 0, { width: verge.viewportW()});
TweenLite.to($('.panel-hide'), 0, { x: -3000, zIndex: 1});

var Animations = {
   slide: function(element, fromDirection, property, duration) {
    var active = $('.panel.active');
    var options;

    if (fromDirection == 'left')
    {
        TweenLite.to(element, 0.0001, { x: -3000 });
        options = {
          x: 0,
          ease: Back.easeInOut
      };
  }
    // move to top
    TweenLite.to(element, 0.0001, { zIndex: 2 });
    // move beneath
    TweenLite.to(active, 0.0001, { zIndex: 1});
    // slide
    TweenLite.to(element, duration, options);

  // TweenLite.to(active, 0, { delay: duration, left: -3000, ease: Linear.easeOut , zIndex: 2});

  active.removeClass('active');
  $(element).addClass('active');
  $(element).siblings('.panel').css('zIndex', 0);
    }
};
