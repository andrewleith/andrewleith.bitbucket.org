function Flatbook(props) {
    var that = this;

    // UI Transitions
    var Animations = {
        ShowSplashScreen:  new TimelineLite({
            tweens: [
                new TweenLite(props.body, 0.75, { autoAlpha: '1' }),
                new TimelineLite({
                    tweens: [
                        // new TweenLite(props.bg, 0.75, { delay: 0.75, autoAlpha: '0', display: 'none' }),
                        // new TweenLite(props.bg2, 0.75, { delay: 0.75, autoAlpha: '1' }),
                        new TweenLite(props.city, 0.65, { delay: 0.75, autoAlpha: '0' })
                    ]
                }),
                new TimelineLite({
                    tweens: [
                        new TweenLite(props.searchbox, 0.5, { delay: 0.5, autoAlpha: '1' }),
                        new TweenLite(props.slogan, 0.5, { delay: 0.5, autoAlpha: '1' })
                    ]
                })
            ]
        }).pause(),

        HideSplashScreen: new TimelineLite({
            tweens: [
                new TweenLite(props.omni, 0.25, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.bg, 0.25, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.citiesFooter, 0.25, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.gmap, 0.25, { delay: 0.25, autoAlpha: '1' }),
                new TweenLite(props.omni2, 0.35, { delay: 0.5, autoAlpha: '1' })
            ]
        }).pause(),

        ShowCities: new TimelineLite({
            tweens: [
                new TweenLite(props.citiesShow, 0.001, { autoAlpha: '0', display: 'none' }),
                new TweenLite(props.citiesHide, 0.001, { autoAlpha: '1' }),
                new TweenLite(props.citiesFooter, 0.25, { top: '40%', delay: 0.001, ease: Linear.easeOut }),
                new TweenLite(props.omni, 0.25, { delay: 0.001, autoAlpha: 0 })
            ]
        }).pause(),

        ShowMenu: new TimelineLite({
            align: 'sequence',
            tweens: [
            new TweenLite([props.omni2], 0.05, {
                autoAlpha: '0', display: 'none'
            }),
            new TimelineLite({
                tweens: [
                new TweenLite(props.logo, 0.25, {
                    autoAlpha: '0'
                }),
                new TweenLite(props.navbar, 0.25, {
                    backgroundColor: 'rgba(47,145,107,0.3)'
                }),
                new TweenLite(props.nav, 0.25, {
                    autoAlpha: '1', display: 'block'
                })
                ]
            })
            ]
        }).pause(),

        ShowFilters: new TimelineLite({
            tweens: [
                TweenLite.from([props.filters], 0.25, { y: '+=-50', ease: Back.easeOut }),
                TweenLite.to([props.filters], 0.25, { autoAlpha: '1', display: 'inline-block' })
            ]
        }).pause(),

        ShowMapResults: new TimelineLite({
            // onComplete: function () {
            //     if (searchResults.listings.length === 0)
            //         props.$selectedListing.hide();
            // },
            tweens: [
                TweenLite.from(props.$resultsbutton, 2, { y: '100%', ease: Elastic.easeOut }),
                new TweenLite(props.$resultsbutton, 0.75, { background: 'rgba(29, 114, 75, 0.75)' }),
                new TweenLite(props.$resultsbutton, 0.5, { autoAlpha: '1', display: 'block' })
            ]
        }).pause(),

        ShowListings: new TimelineLite({
            onStart: function () {
                props.$listingsResults.isotope({
                    layoutMode: 'masonry',
                    itemSelector: '.fb-card',
                    masonry: {
                        columnWidth: 20,
                        gutterWidth: 10
                    }
                });
            },
            tweens: [
                new TweenLite(props.listings, 0.5, { opacity: '1' }),
                new TweenLite(props.listings, 0.001, { zIndex: '2' }),
                new TweenLite(props.selectedListing, 0.25, { autoAlpha: '0' }),
                new TweenLite(props.selectedListing, 0.001, { delay: 0.25, zIndex: -1 }),
                new TweenLite(props.navbar, 0.5, {backgroundColor: 'rgba(0, 0, 0, 0.5)' }),
                TweenLite.from(props.mapbutton, 0.25, { delay: 0.5, y: '-50px', ease: Back.easeOut, immediateRender: false }),
                new TweenLite(props.mapbutton, 0.25, { delay: 0.5, autoAlpha: '1', display: 'block' })
            ]
        }).pause(),

        ShowListing: new TimelineLite({
            onComplete: function () {
                // run scrollspy now to fix position: relative issues
                $('[data-spy="scroll"]').each(function () {
                    var $spy = $(this).scrollspy('refresh');
                });
            },
            onReverseComplete: function() {
                props.$listingbutton.find('a').html('<span class="glyphicon glyphicon-list"></span> back to listings</a>');
            },
            tweens: [
                new TweenLite(props.listing, 0.001, { display: 'block' }),
                new TweenLite(props.listing, 0.15, { autoAlpha: '1' }),
                new TweenLite(props.listingbutton, 0.0001, { display: 'block' }),
                TweenLite.from(props.listingbutton, 0.25, { delay: 0.5, y: '-50px', ease: Back.easeOut }),
                new TweenLite(props.listingbutton, 0.25, { delay: 0.5, autoAlpha: '1' }),
                new TweenLite(props.navbar, 0.25, { backgroundColor: 'rgba(0, 0, 0, 0.5)'})
            ]
        }).pause()

    };


    // -------------------------------
    // priviledged methods - main business logic

    // search for listings by cityId
    this.Search = function(cityId) {

    };

    // narrow results on the map and listings screens
    this.Applyfilters = function(checkin, checkout, guests) {

    };

    // Display details of a listing
    this.ShowListing = function(listingId) {

    };

    // Book a listing
    this.BookListing = function(listingId) {

    };

    // Show cities list
    this.citiesShown = false;
    this.ToggleCities = function() {
    };

    // Show menu
    this.menuShown = false;
    this.ToggleMenu = function() {

    };

    // END priviledged methods
    // -------------------------------


    // -------------------------------
    // Private properties

    // get city list
    var cities;
    var listings;

    // -------------------------------
    // Event Bindings

    /* search */
    props.$search.on('click', function() {
        this.Search();
    });
    props.$search2.on('click', onSubsequentSearchClick);
    props.$keywords.keyup(function (e) { onInitialSearchKeyPress(e); });
    props.$keywords2.keyup(function (e) { onSubsequentSearchKeyPress(e); });
    props.$drawerbutton.on('click', function () { toggleFilters(); });


    /* cities */
    props.$citiesBtn.on('click', toggleCities);


    // misc
    // city autocomplete
    var typeaheadOptions = {
        matcher: function (item) {
            if (item.toLowerCase().indexOf(this.query.toLowerCase()) !== -1) return true;
            return false;
        },
        showHintOnFocus: true,
        source: that.cities.typeaheadList,
        autoselect: true,
        updater: function (item) {
            console.log('updating!: ' + arguments[0]);
            if (that.cities.isValid(item)) {
                props.$error.addClass('hiddenfb');
            }
            else {
                props.$error.removeClass('hiddenfb');
            }
            return item;
        }
    };
    props.$keywords.typeahead(typeaheadOptions);
    props.$keywords2.typeahead(typeaheadOptions);

    // calendars
    props.$filterCheckin.datepicker(dateOptions).on('changeDate', function (e) {
        props.$filterCheckin.datepicker('hide');
        props.$filterCheckout.datepicker('show');
        filterListings();
    });
    props.$filterCheckout.datepicker(dateOptions).on('changeDate', function (e) {
        props.$filterCheckout.datepicker('hide');
        filterListings();
    });


    // -------------------------------
    // Startup Code

}

function Cities(url) {

    if (url) {
        $.get()
    }

}