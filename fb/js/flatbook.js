var MOBILE_BREAK_POINT = 768;
var SPLASH_IMG = 'img/bg2.jpg';

function Flatbook(props) {
    var that = this;
    var mobile = false; // i apologize
    var filterOptions;
    this.starting = true;

    //   this.props = props;

    // position some stuff
    TweenLite.set([props.listingbutton, props.resultsbutton, props.mapbutton], {y: -200 });

    this.Animations = {
        ShowSplashScreen:  new TimelineLite({
            tweens: [
                new TweenLite(props.body, 0.75, { autoAlpha: '1' }),
                new TimelineLite({
                    tweens: [
                        new TweenLite(props.city, 0.65, { delay: 0.75, autoAlpha: '0' })
                    ]
                }),
                new TimelineLite({
                    tweens: [
                        new TweenLite(props.searchbox, 0.5, { delay: 0.5, autoAlpha: '1' }),
                        new TweenLite(props.slogan, 0.5, { delay: 0.5, autoAlpha: '1' })
                    ]
                })
            ]
        }).pause(),

        HideSplashScreen: new TimelineLite({
            onStart: function() {
                props.$body.removeClass('splash');
            },
            tweens: [
                TweenLite.set(props.logo, {position: 'absolute'}),
                new TweenLite(props.gmap, 0.5, { delay: 0.25, autoAlpha: '1' }),
                new TweenLite([props.logo, props.omni, props.bg, props.citiesFooter, props.holder2], 0.5, { autoAlpha: '0', display: 'none' }),
                new TweenLite([props.logo2, props.omni2], 0.35, { delay: 0.5, autoAlpha: '1', display: 'block' }),
                TweenLite.set(props.listings, {overflowY: 'scroll'})
            ]
        }).pause(),

        HideSplashScreenMobile: new TimelineLite({
            onStart: function() {
                props.$body.removeClass('splash');
                props.$omni2.attr('style', ''); // not sure why this is necessary, animations are fighting with each other
            },
            tweens: [
                TweenLite.to([props.omni, props.logo, props.bg, props.citiesFooter, props.holder2], 0.5, { x: -3000, ease: Expo.easeOut, immediateRender: false }),
                new TweenLite([props.omni, props.logo, props.bg, props.citiesFooter, props.holder2], 0.25, { autoAlpha: '0', display: 'none' }),
                new TweenLite([props.logo2, props.omni2], 0.35, { delay: 0.5, autoAlpha: '1' })
            ]
        }).pause(),

        ShowCities: {
            play: function() {
                TweenLite.set(props.citiesFooter, { autoAlpha: 0, zIndex: 100});
                TweenLite.to(props.citiesFooter, 0.5, {autoAlpha: 1});
                
                $(props.citiesFooter + ' li, ' + props.citiesFooter + ' h3').each(function() {
                    TweenLite.set(this, { autoAlpha: 1});
                    TweenLite.from(this, 0.3, {delay:Math.random() * 0.5, y:-200, autoAlpha:0, scale:4});
                });
            },

            reverse: function() {
                TweenLite.to(props.citiesFooter, 0.3, {autoAlpha: 0});
                TweenLite.set(props.citiesFooter, {delay: 0.3, zIndex: -1});
            }
        },

        ShowMenu: new TimelineLite({
            align: 'sequence',
            tweens: [
            new TweenLite([props.omni2], 0.05, { autoAlpha: '0'}),
            new TimelineLite({
                tweens: [
                    new TweenLite([props.omni, props.logo], 0.25, { autoAlpha: '0', display: 'none'}),
                    new TweenLite(props.navbar, 0.25, { delay: 0.25, backgroundColor: 'rgba(47,145,107,0.3)', height: 'auto' }),
                    new TweenLite(props.nav, 0.25, { delay: 0.25, autoAlpha: '1', display: 'block' })
                ]
            })
            ]
        }).pause(),

        ShowFilters: new TimelineLite({
            tweens: [
                TweenLite.from([props.filters], 0.25, { y: '+=-50', ease: Back.easeOut }),
                TweenLite.to([props.filters], 0.25, { autoAlpha: '1', display: 'block' })
            ]
        }).pause(),

        ShowMapResults: new TimelineLite({
            onComplete: function () {
                if (that.cities.currentResults().listings.length === 0)
                    props.$selectedListing.hide();
                //that.search.toggleFilters();
            },
            tweens: [
                new TweenLite(props.body, 0.75, { autoAlpha: '1' }),
                new TweenLite(props.resultsbutton, 2, { y: 0, ease: Elastic.easeOut }),
                new TweenLite(props.resultsbutton, 0.5, { autoAlpha: '1', display: 'block' }),
                new TimelineLite({
                    tweens: [new TweenLite(props.$resultsbutton, 0.5, { delay: 0.75, display: 'block' })],
                    onComplete: function() {
                        that.search.showFilters();
                    }
                })
            ]
        }).pause(),

        ShowListings: new TimelineLite({
            onStart: function () {
                // props.$listingsResults.isotope({
                //     layoutMode: 'masonry',
                //     itemSelector: '.fb-card',
                //     masonry: {
                //         columnWidth: 20,
                //         gutterWidth: 10
                //     }
                // });
                that.search.hideFilters();
            },
            onComplete: function() {
                TweenLite.set(props.listings, { overflowY: 'scroll', immediateRender: false });
            },
            tweens: [
                new TweenLite(props.selectedListing, 0.25, { autoAlpha: '0', zIndex: -1, immediateRender: false }),
                TweenLite.set(props.listings, { autoAlpha: '1', zIndex: '2', immediateRender: false }),
                TweenLite.from(props.listings, 0.5, { x: 3000, ease: Expo.easeOut, immediateRender: false }),
                new TweenLite(props.navbar, 0.75, {backgroundColor: 'rgba(0, 0, 0, 0.5)', immediateRender: false }),
                new TweenLite(props.mapbutton, 0.25, { delay: 0.75, y: '0', ease: Back.easeOut, immediateRender: false }),
                new TweenLite(props.mapbutton, 0.25, { delay: 0.75, autoAlpha: '1', display: 'block', immediateRender: false })
                // new TweenLite(props.logo, 0.75, { autoAlpha: '1', display: 'block', immediateRender: false })
            ]
        }).pause(),

        ShowListingsMobile: new TimelineLite({
            onStart: function() {
                document.activeElement.blur();
            },
            tweens: [
                new TweenLite(props.selectedListing, 0.25, { autoAlpha: '0', zIndex: -1, immediateRender: false }),
                TweenLite.set(props.listings, { autoAlpha: '1', zIndex: '2', immediateRender: false }),
                TweenLite.from(props.listings, 0.75, { x: 900, ease: Expo.easeOut, immediateRender: false }),
            ]
        }).pause(),

        ShowListing: new TimelineLite({
            onComplete: function () {
                // run scrollspy now to fix position: relative issues
                $('[data-spy="scroll"]').each(function () {
                    var $spy = $(this).scrollspy('refresh');
                });
            },
            onReverseComplete: function() {
                props.$listingbutton.find('a').html('<span class="glyphicon glyphicon-list"></span> back to listings</a>');
            },
            tweens: [
                TweenLite.set(props.listing, { autoAlpha: '1'}),
                TweenLite.from(props.listing, 0.5, { x: 3000, ease: Expo.easeOut}),
                new TweenLite(props.listingbutton, 0.25, { delay: 0.75, autoAlpha: '1', y: 0, ease: Back.easeOut }),
                new TweenLite(props.navbar, 0.25, { backgroundColor: 'rgba(0, 0, 0, 0.5)'})
            ]
        }).pause(),

        ShowListingMobile: new TimelineLite({
            onComplete: function () {
                // run scrollspy now to fix position: relative issues
                $('[data-spy="scroll"]').each(function () {
                    var $spy = $(this).scrollspy('refresh');
                });
            },
            onReverseComplete: function() {
                props.$listingbutton.find('a').html('<span class="glyphicon glyphicon-list"></span> back to listings</a>');
            },
            tweens: [
                new TweenLite(props.listing, 0.75, {x: 0,ease: Expo.easeOut  }),
                // new TweenLite(props.listing, 0.001, { display: 'block' }),
                // new TweenLite(props.listing, 0.15, { autoAlpha: '1' }),
                //new TweenLite(props.listingbutton, 0.0001, { display: 'block' }),
                new TweenLite(props.listingbutton, 0.25, { delay: 0.75, autoAlpha: '1', y: 0, ease: Back.easeOut }),
                new TweenLite([props.menu, props.omni2], 0.25, { autoAlpha: '0'})
            ]
        }).pause()

    };
    var Animations = this.Animations; //wtf is that
    
    this.hideSplash = function() {
        if (mobile)
            Animations.HideSplashScreenMobile.play(0);
        else
            Animations.HideSplashScreen.play(-1.25);
    };

    this.search = (function () {
        // other ui plugins
        var l = Ladda.create(document.querySelector(props.search));


        // ui event bindings

        // SEARCH EVENT - search button click, enter keypress
        // props.$search.on('click', initApp);
        // props.$search2.on('click', initApp);
        // props.$keywords.keyup(function (e) { onInitialSearchKeyPress(e); });
        // props.$keywords2.keyup(function (e) { onSubsequentSearchKeyPress(e); });
        // props.$keywords.on('mouseup', focusKeywords);
        props.$keywords2.on('mouseup', focusKeywords);


        // filters
        props.$keywords2.on('focus', toggleFilters);
        props.$closeDrawerbutton.on('click', toggleFilters);
       
        // SEARCH EVENT - events to attach to
        function focusKeywords() {
            $(this).select();
        }
        function initApp(delay) {

            // reset map
            //that.googlemaps.map.panTo(new google.maps.LatLng(0, 0));

            if (that.cities.isValid(props.$keywords.val())) {

                // history
                var cityId = that.cities.GetCityByName(props.$keywords.val()).id;
                that.addState({ state: that.States.explore, path: '/' + cityId, data: { keywords: props.$keywords.val() } });

                search(delay);
                
            }
            
        }
        function search(delay) {
            l.start();
            that.googlemaps.map.panBy(10,10);
            google.maps.event.addListenerOnce(that.googlemaps.map, 'center_changed', function () {
                setTimeout(function () {
                    
                    Animations.HideSplashScreen.eventCallback("onComplete", function() {
                        props.$bg.remove();

                        ShowApp(that.googlemaps.showMapResults);
                    });

                    if (that.starting)
                        Animations.HideSplashScreen.play(0);

                    console.log("search!");

                    that.starting = false;
                    
                }, delay);
                
                
            });

            // copy keywords to main app
            if (props.$keywords2.val() === '') {
                props.$keywords2.val(props.$keywords.val());

            }

            // perform search
            that.cities.searchByName(props.$keywords2.val());

            that.googlemaps.center();

        }
        function onInitialSearchKeyPress(e) {
            if (e.keyCode == 13) {
                initApp();
            }
        }
        function onSubsequentSearchKeyPress(e) {
            if (e.keyCode == 13) {
               initApp();
            }
        }


        //props.$filtersbutton.on('click', toggleFilters);

        // search business logic
        // ----------------------
        // animate interaction
        function ShowApp(showMapResults) {
            that.menu.hideMenu();
            that.menu.inIntro = false;

            // unfocus the search boxes to hide mobile keyboards
                        
            mobile = isMobile();
            props.$omni2.removeClass('hiddenfb');

            // Mobile Mode
            if (mobile) {
                Animations.HideSplashScreenMobile.play(-1);
                that.listings.showListings();

                return;
            }

            // Desktop Mode
        
            
            showMapResults();

            //show card
            if (that.cities.currentResults().listings.length > 0) {
                TweenLite.set(props.selectedListing, { display: 'block' });
                new TweenLite(props.selectedListing, 0.25, { delay: 0.75, autoAlpha: '1' });
            }
        }



        var filtersShown = false;

        function hideFilters() {
            if (filtersShown) {
                Animations.ShowFilters.reverse(0);
                filtersShown = false;
                props.$drawerbutton.removeClass('active');
            }
        }

        function showFilters() {
            if (!filtersShown) {
                Animations.ShowFilters.play(0);
                filtersShown = true;
                props.$drawerbutton.addClass('active');
            }
        }

        function toggleFilters() {
            if (filtersShown) {
                hideFilters();
            } else {
                showFilters();
            }

        }


        return {
            showFilters: showFilters,
            hideFilters: hideFilters,
            toggleFilters: toggleFilters,
            go: initApp,
            search: search
        };
    } ());


    this.cities = (function () {

        // CITIES EVENTS - cities button click
        props.$cities.on('click', toggleCities);
        props.$citiesClose.on('click', toggleCities);
        $(document).on('click', props.citiesFooter + ' a', function () {
            

            var h = this.href.substr(this.href.indexOf('#/'));
            var cityId = h.substr(h.lastIndexOf('/')+1);
            var city = that.cities.GetCityById(cityId);

            that.addState({ state: that.States.explore, path: '/' + cityId, data: { keywords: props.$keywords.val() } });

            props.$keywords.val(city.name + ", " + city.country);
            that.cities.hideCities();
            that.search.search(2000);

            return false;
        });
        // cities toggle
        var citiesShown = false;

        function hideCities() {
            if (citiesShown) {
                Animations.ShowCities.reverse(0);
                //                TweenLite.set(props.holder2, {zIndex: 200, autoAlpha: 0.5});
                //                TweenLite.to([props.omni, holder2], 0.5, {autoAlpha: 1});
                //                TweenLite.to([props.citiesFooter], 0.5, {autoAlpha: 0.25});
                citiesShown = false;
            }
        }

        function showCities() {

            if (!citiesShown) {
                Animations.ShowCities.play(0);
                citiesShown = true;
            }
        }

        function toggleCities() {
            if (citiesShown) {
                hideCities();
            } else {
                showCities();
            }

        }

        // business logic
        // ----------------------

        var currentCity = "";
        var cityList;
        jQuery.support.cors = true; //fixes ie9 sucking
        var __reqCities = $.ajax({
            dataType: "json",
            url: 'https://flatbook.co/api/getcities',
            async: false

        });
        var cities = __reqCities.responseJSON;

        // re-organize cities by country

        var citiesByCountry = [];
        var countries = {};
        var unique = {};
        var distinct = [];

        for (var i in cities) {
            if (typeof (unique[cities[i].country]) == "undefined") {
                distinct.push(cities[i].country);
            }
            unique[cities[i].country] = 0;
        }

        for (var j in distinct) {
            citiesByCountry.push({ name: distinct[j], cities: cities.filter(countryFilter) });
        }
        var tmplObj = {
            countries: citiesByCountry
        };

        // write them to the cities list
        props.$citiesList.html(Mustache.to_html(props.$citiesTpl.html(), tmplObj));

        function countryFilter(elem) {
            return elem.country == distinct[j];
        }
        function listCities() {
            return cities;
        }
        function GetCityById(id) {
            return cities.filter(function (elem) { return elem.id == id; })[0];
        }
        function GetCityByName(name) {
            return cities.filter(function (elem) { return elem.name + ", " + elem.country === name; })[0];
        }
        function onUpdateTypeahead(item) {
            console.log('updating!: ' + arguments[0]);

            if (that.cities.isValid(item)) {

                props.$error.addClass('hiddenfb');
                props.$keywords.val(item);
                props.$keywords2.val(item);
                that.search.go(2000);

            }
            else {
                props.$error.removeClass('hiddenfb');
            }
            return item;
        }
        function typeaheadList() {
            return __reqCities.responseJSON.map(function (item) { return item.name + ", " + item.country; });
        }

        function listCurrentCoords() {
            if (currentCity !== "") {
                return [currentCity.latitude, currentCity.longitude];
            }

            else
                return [0, 0];
        }

        function isValid(value) {

            var city = cities.filter(function (el) { return el.name + ", " + el.country == value; });
            result = city.length > 0;

            if (result) {
                props.$error.addClass('hiddenfb');
                props.$error2.addClass('hiddenfb');
                //currentCity = city[0];
            } else {
                props.$error.removeClass('hiddenfb');
                props.$error2.removeClass('hiddenfb');
            }

            return result;
        }


        function searchByName(name) {
            search(GetCityByName(name).id);
        }
        var currentResults;
        function search(id) {

            var listings = {
                listings: [
                {
                    id: '0',
                    title: 'The Plateau Experience',
                    shortDescription: 'Situated right next to the Parc Mont-Royal, The Plateau Experience is the ideal place for a quiet getaway while still be close to the action',
                    price: '$250',
                    imageUrls: ['img/thumb1.jpg'],
                    location: "Le plateau, Montreal",
                    description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                    amenities: '',
                    beds: '2',
                    baths: '1',
                    accomodates: '5',
                    rules: '',
                    reviews: '',
                    coords: [45.41846, -75.69788],
                    available: [
                        { start: '12/26/2013', end: '01/07/2014' },
                        { start: '01/31/2014', end: '02/28/2014' }
                    ],
                    booked: [
                        { start: '01/06/2014', end: '01/30/2014' }
                    ]
                },
                {
                    id: '1',
                    title: 'Two Story, 2BR Plateau Apartment',
                    shortDescription: 'Located right beside the famous Mont-Royal Avenue, you will find many little cafés, restaurants, boutiques and a truly charming ambiance.',
                    price: '$250',
                    imageUrls: ['img/thumb2.jpg'],
                    location: "Le plateau, Montreal",
                    description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                    amenities: '',
                    beds: '3',
                    baths: '2',
                    accomodates: '7',
                    rules: '',
                    reviews: '',
                    coords: [45.41273, -75.68655],
                    available: [
                        { start: '12/26/2013', end: '01/05/2014' },
                        { start: '01/31/2014', end: '02/28/2014' }
                    ],
                    booked: [
                        { start: '01/06/2014', end: '01/30/2014' }
                    ]
                },
                {
                    id: '2',
                    title: 'Beautiful 1BR in the Plateau',
                    shortDescription: 'Located right beside the famous Mont-Royal Avenue, you will find many little cafés, restaurants, boutiques and a truly charming ambiance.',
                    longDescription: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                    price: '$250',
                    imageUrls: ['img/thumb3.jpg'],
                    location: "Le plateau, Montreal",
                    description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                    amenities: '',
                    beds: '1',
                    baths: '1',
                    accomodates: '2',
                    rules: '',
                    reviews: '',
                    coords: [45.40532, -75.69865],
                    available: [
                        { start: '12/26/2013', end: '01/05/2014' },
                        { start: '01/31/2014', end: '02/28/2014' }
                    ],
                    booked: [
                        { start: '01/06/2014', end: '01/30/2014' }
                    ]
                },
                {
                    id: '3',
                    title: 'The Plateau Experience',
                    shortDescription: 'Situated right next to the Parc Mont-Royal, The Plateau Experience is the ideal place for a quiet getaway while still be close to the action',
                    price: '$250',
                    imageUrls: ['img/thumb4.jpg'],
                    location: "Le plateau, Montreal",
                    description: 'Special offer for August 2013! Now $159 CAD/night! This wonderfully located apartment has everything to make your Montreal experience authentic and memorable. Clean and fine-looking, our apartment is situated in the Mile-End, Montreal\'s most appealing neighbourhood. Filled with splendid cafés, patrimonial theatres, top-rated restaurants, trendy boutiques and state of the art galleries, the lively Mile-End is Montreal\'s cultural icon. Our apartment is as close as can be to the famous Parc Mont-Royal, where you may relax and enjoy a spectacular view of the city or join in on a friendly baseball or volleyball match with the locals. You will also find, a short walk away, grocery stores, bakeries, fish shops and wine stores. Montreal\'s most attended bus line, on Avenue Du Parc (100 meters away), will take you downtown in less than 10 minutes. Inside our apartment, you will find everything you need to make your stay comfortable and enjoyable. You will find at your disposal a barbecue, a fondue set, full kitchenware and cocktail, wine and low-ball glasses. A Washer and a dryer as well as a dishwasher will be functional during your stay. Please do not hesitate to contact us if you have any questions! We look forward to meeting you!',
                    amenities: '',
                    beds: '2',
                    baths: '1',
                    accomodates: '4',
                    rules: '',
                    reviews: '',
                    coords: [45.40394, -75.69530],
                    available: [
                        { start: '12/26/2013', end: '01/05/2014' },
                        { start: '01/31/2014', end: '02/28/2014' }
                    ],
                    booked: [
                        { start: '01/06/2014', end: '01/30/2014' }
                    ]
                }
                ]
            };

            currentResults = listings;
            currentCity = cities.filter(function (elem) { return elem.id == id; })[0];
            props.$keywords2.val(currentCity.name + ", " + currentCity.country);
            // update results number
            props.$numresults.html(currentResults.listings.length);
            // update listings
            props.$listingsResults.html(Mustache.to_html(props.$listingsTpl.html(), currentResults));

            return currentResults;
        }

        // return
        return {
            listCities: function () {
                return (listCities());
            },
            currentCity: function () {
                return currentCity;
            },
            listCurrentCoords: listCurrentCoords,
            isValid: isValid,
            hideCities: hideCities,
            showCities: showCities,
            toggleCities: toggleCities,
            typeaheadList: typeaheadList,
            onUpdateTypeahead: onUpdateTypeahead,
            citiesByCountry: citiesByCountry,
            GetCityById: GetCityById,
            GetCityByName: GetCityByName,
            search: search,
            searchByName: searchByName,
            currentResults: function () { return currentResults; }
        };
    } ());

    this.googlemaps = (function () {

        /* maps */
        var infowindow, map;
        props.map = map;
        var gray = [{
            "stylers": [{
                "hue": "#ff1a00"
            }, {
                "invert_lightness": true
            }, {
                "saturation": -100
            }, {
                "lightness": 33
            }, {
                "gamma": 0.5
            }]
        }, {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{
                "color": "#2D333C"
            }]
        }];


        // center map by default
        var centerdef = new google.maps.LatLng(0, 0);
        var mapOptions = {
            draggable: true,
            zoom: 13,
            mapTypeControl: false,
            panControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: false,
            streetViewControl: false,
            scrollwheel: true
        };
        var GMAPOptions = {
            div: '#map-canvas',
            zoom: 13,
            lat: 0,
            lng: 0,
            mapTypeControl: false,
            panControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: false,
            streetViewControl: false,
            scrollwheel: true,
            click: function() {
                that.search.hideFilters();
            },
            dragstart: function() {
                that.search.hideFilters();
            }
        };

        // GMAPS stuff
        var MapWrapper = new GMaps(GMAPOptions);
        map = MapWrapper.map;
        MapWrapper.addStyle({
            styledMapName: "Styled Map",
            styles: gray,
            mapTypeId: "map_style"
        });
        MapWrapper.setStyle("map_style");

        // display map results

        var markers = [], searchResults, ids = [];
        function showMapResults() {
            console.log('debug: call showMapResults');

            props.$selectedListing.hide();

            // GMaps implementation
            MapWrapper.removeMarkers();
            $('.overlay').remove();

            // add markers and info windows for each result
            searchResults = that.cities.currentResults();
            for (var j = 0; j < searchResults.listings.length; j++) {
                var listing = searchResults.listings[j];

                var m = MapWrapper.addMarker({
                    lat: listing.coords[0],
                    lng: listing.coords[1],
                    animation: google.maps.Animation.DROP,
                    //infoWindow: { content: Mustache.to_html(props.$infowindowTpl.html(), searchResults.listings[j])},
                    details: {
                        id: listing.id.toString()
                    },
                    click: markerClick,
                    icon: (j === 0 ? 'img/marker-default.png' : 'img/marker.png')
                });

                if (j === 0) {
                    selectedMarker = m;
                    props.$selectedListing.html(Mustache.to_html(props.$listingTpl.html(), searchResults.listings[j]));
                }
            }

            // reset zoom, enable all map controls
            var newmapOptions = {
                draggable: true,
                zoom: 13,
                mapTypeControl: false,
                panControl: false,
                zoomControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControl: false,
                streetViewControl: false,
                scrollwheel: true
            };
            map.setOptions(newmapOptions);
           
            Animations.ShowMapResults.play(0);

            
        }

        var selectedMarker;
        function markerClick() {
            if (typeof selectedMarker !== "undefined") {
                selectedMarker.setIcon('img/marker.png');
            }
            selectedMarker = this;
            this.setIcon('img/marker-default.png');

            // update infowindow thing
            var currentId = this.details.id;
            var listing = searchResults.listings.filter( function(el) { return el.id == currentId; })[0];
            props.$selectedListing.html(Mustache.to_html(props.$listingTpl.html(), listing));
        }

        function nextCard() {
            var current = MapWrapper.markers.indexOf(selectedMarker);
            var next = current + 1;

            if (next === MapWrapper.markers.length) {
                next = 0;
            }

            var nextMarker = MapWrapper.markers[next];
            markerClick.call(nextMarker);
        }
        function prevCard() {
            var current = MapWrapper.markers.indexOf(selectedMarker);
            var prev = current - 1;

            if (prev === -1) {
                prev = MapWrapper.markers.length - 1;
            }

            var prevMarker = MapWrapper.markers[prev];
            markerClick.call(prevMarker);
        }



        function cardClick() {
            var myid = $(this).closest('.fb-card').attr('id');
            var current = myid.substring(myid.indexOf('-') + 1);
            var listing = that.cities.currentResults().listings.filter( function(el) { return el.id == current; })[0];
            props.$listingResult.html(Mustache.to_html(props.$listingResultTpl.html(), listing));

            that.listings.toggleListing();

            return false;
        }
        function cardClickNoMap() {
            var myid = $(this).closest('.fb-card').attr('id');
            var current = myid.substring(myid.indexOf('-') + 1);
            var listing = searchResults.listings.filter( function(el) { return el.id == current; })[0];
            props.$listingResult.html(Mustache.to_html(props.$listingResultTpl.html(), listing));
            props.$listingbutton.find('a').html('<span class="glyphicon glyphicon-map-marker"></span> back to map</a>');
            
            that.listings.toggleListing();

            return false;
        }


        function filterMarkers(searchResults) {
            var filterResults = searchResults.listings;
            for (var j = 0; j < MapWrapper.markers.length; j++) {
                thisid = MapWrapper.markers[j].details.id;
                if (filterResults.filter(filterMarker).length === 0)
                    MapWrapper.markers[j].setVisible(false);
                else
                    MapWrapper.markers[j].setVisible(true);
            }
        }
        var thisid;
        function filterMarker(el) {
            return el.id === thisid;
        }

        function center() {
            var latLng = that.cities.listCurrentCoords();
            map.panTo(new google.maps.LatLng(latLng[0], latLng[1]));
        }




        // EVENT BINDINGS
        // bind listing click event once
        $(document).on('click', '#fb-listings .fb-card .iw-book', cardClick);
        $(document).on('click', '#fb-selected-listing .fb-card .iw-booknow', cardClickNoMap);
        $(document).on('click', '#fb-selected-listing ' + props.cardNext, nextCard);
        $(document).on('click', '#fb-selected-listing ' + props.cardPrev, prevCard);

        

        // on listing close
        props.$listingbutton.on('click',  function() {
            that.listings.toggleListing();
            return false;
        });


        // filtering
        $(document).on('change', props.filterBeds + ',' + props.filterBaths + ',' + props.filterCheckin + ',' + props.filterCheckout, filterListings);

        // calendars setup
        var firstRun = true;
        var $rs = $('.range-start');
        var $re = $('.range-end');

        $('.range').datepicker({
            inputs: $('.range-start, .range-end')
        });

        $('.range-start').datepicker().on('changeDate', function(e) {

            // remove end-date selection on first run - not sure why it gets set
            if (firstRun) {
                $re.find('.day.selected.active').removeClass('selected').removeClass('active');
                $re.find('.day.range').removeClass('range');
                // $re.datepicker('setDate', e.date);
                // $re.datepicker('update', e.date);
                $re.datepicker('clear');
                props.$filterCheckin.removeClass("active");
                props.$filterCheckout.addClass("active");
                $re.datepicker('update', e.date);
                $re.val('');
                firstRun = false;
            }

            props.$filterCheckin.val(e.format());
            $rs.hide();
            $re.show();
            filterListings();
        });

        $('.range-end').datepicker().on('changeDate', function(e) {
            
            props.$filterCheckout.removeClass("active");

            if (e.date <= parseDate(props.$filterCheckin.val())) {
                props.$filterCheckin.val(e.format());
                $re.datepicker('clear');
            }
            else {
                props.$filterCheckout.val(e.format());
            }
            filterListings();
        });
        props.$filterCheckin.on('focus', function() {
            $rs.show();
            $re.hide();
        });
        props.$filterCheckout.on('focus', function() {
            $rs.hide();
            $re.show();
        });

        // clear default selected dates
        $re.hide();
        $re.datepicker('clear');
        $rs.datepicker('clear');
        $rs.find('.day.selected.active').removeClass('selected').removeClass('active');
        
        // number of guests click
        $('.badge').on('click', function() {
            var me = $(this).siblings('.badge').removeClass('active').length + 1;
            props.$filterGuests.val(me);
            $(this).addClass('active');
        });

        function filterListings() {
            that.filterOptions = { beds: props.$filterBeds.val(), baths: props.$filterBaths.val(), availability: { start: props.$filterCheckin.val(), end: props.$filterCheckout.val()} };

            var value = $('#fb-listings .fb-card').addClass('hiddenfb').filter(function (index) {
                var $this = $(this);
                var currentValues = {
                    beds: parseInt($this.data('beds'), 10),
                    baths: parseInt($this.data('baths'), 10),
                    availStart: $this.data('avail-start').split(','),
                    availEnd: $this.data('avail-end').split(',')
                };

                var avail = [];
                for (var i = 0; i < currentValues.availStart.length - 1; i++) {
                    avail.push({ start: currentValues.availStart[i], end: currentValues.availEnd[i] });
                }

                var availFilter = avail.filter(availabilityFilter).length > 0;

                return availFilter;
            });

            value.removeClass("hiddenfb");

            // update map markers
            $('.fb-card').each(function () {
                var id = this.id.substring(this.id.indexOf('-') + 1);

                if ($(this).hasClass('hiddenfb')) {
                    MapWrapper.markers[id].setVisible(false);
                }
                else {
                    MapWrapper.markers[id].setVisible(true);
                }
            });

            //filterMarkers(that.cities.currentResults());
        }

        function availabilityFilter(el) {
            var start = (that.filterOptions.availability.start === '' ? true : parseDate(that.filterOptions.availability.start) >= parseDate(el.start));
            var end = (that.filterOptions.availability.end === '' ? true : parseDate(that.filterOptions.availability.end) <= parseDate(el.end));
            return start && end;
        }

        function hideInfoWindows() {
            // for (var i = 0; i < that.googlemaps.MapWrapper.markers.length; i++) {
            //     MapWrapper.markers[i].infoWindow.close();
            // }
        }
        return {
            map: map,
            showMapResults: showMapResults,
            center: center,
            filterMarkers: filterMarkers,
            filterListings: filterListings,
            MapWrapper: MapWrapper,
            hideInfoWindows: hideInfoWindows
        };
    } ());

    this.menu = new Menu();

    this.listings = (function () {
        var currentResults;

        // listings toggle
        var listingsShown = false;
        var $icon = props.$resultsbutton.find('.glyphicon');
        function hideListings() {
            if (listingsShown) {
                if (mobile) {
                    Animations.ShowListingsMobile.reverse(0.25, false);
                }
                else {
                    Animations.ShowListings.reverse(0.25, false);
                    TweenLite.set(props.listings, { overflowY: 'hidden', immediateRender: false });
                }

                listingsShown = false;
                // $icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
        }
        function showListings(onCompleteTimeline) {

            if (mobile) {
                that.search.hideFilters();
            }

            if (!listingsShown) {
                if (mobile) {
                    Animations.ShowListingsMobile.play(-1.5, false);
                }
                else
                    Animations.ShowListings.play(0, false);
                
                listingsShown = true;
                // $icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        }
        function toggleListings() {
            if (listingsShown) {
                that.removeLastState();
                hideListings();
            } else {
                // if (that.CurrentState.state !== that.States.list)
                that.addState({ state: that.States.list });
                
                showListings();
            }

            return false;

        }

        // listing toggle
        var listingShown = false;
        function hideListing() {
            if (listingShown) {
                if (mobile)
                    Animations.ShowListingMobile.reverse(0.05);
                else
                    Animations.ShowListing.reverse(0.25);

                listingShown = false;
            }
        }
        function showListing(onCompleteTimeline) {

            if (mobile) {
                that.search.hideFilters();
            }

            if (!listingShown) {
                if (mobile)
                    Animations.ShowListingMobile.play(0);
                else
                    Animations.ShowListing.play(0);
                
                listingShown = true;
            }
        }
        function toggleListing() {
            if (listingShown) {
                that.removeLastState();
                hideListing();
            }
            else {
                // if (that.CurrentState.state !== that.States.view)
                that.addState({ state: that.States.view });
                
                showListing();
            }

            return false;
        }

        // ui event bindings
        props.$resultsbutton.on('click', toggleListings);
        props.$mapbutton.on('click', toggleListings);


        //var filterOptions = {beds: '2', baths: '', availability: { start: '12/26/2013' } };

        $('.fb-li-menu a').on('click', function () {
            $('#' + this.href.substring(this.href.indexOf('#') + 1)).ScrollTo();
            event.preventDefault();
        });

        return {
            showListings: showListings,
            hideListings: hideListings,
            toggleListings: toggleListings,
            showListing: showListing,
            hideListing: hideListing,
            toggleListing: toggleListing,
            listingsShown: function () { return listingsShown; },
            currentResults: function () { return currentResults; }
        };
    } ());




    /* back/forward button support */
    this.CancelStartup = false;
    this.States = {
        start: 'start',
        explore: 'explore',
        list: 'list',
        view: 'view',
        menu: 'menu'
    };

    this.AppState = [];
    this.AppDrivenStateChange = false;
    this.LastState = null;
    this.CurrentState = function() {
        return AppState[AppState.length -1 ];
    };
    this.addState = function(state) {
        this.AppDrivenStateChange = true;

        if (state.path === undefined)
            state.path = '';

        this.AppState.push(state);
        window.location.hash = '#/' + state.state + state.path;
    };

    this.removeLastState = function() {
        this.AppDrivenStateChange = true;

        if (this.AppState.length > 0) {
            this.AppState.pop();
            var state = this.AppState[this.AppState.length -1];
            
            if (state.path === undefined)
                state.path = '';
            
            window.location.hash = '#/' + state.state + state.path;
        }
    };
    //this.AppState.push({state: 'start'});

    function GetLastRoute() {
        var from;
        
        if (that.AppState[that.AppState.length -1] === undefined) {
            from = 'NONE';
        }
        else {
            from = that.AppState[that.AppState.length - 1].state;    
        }
            
        return from;

    }

    Sammy(function () {
        var from;
        this.before(/.*/, function() {
            from = GetLastRoute();

            // from #view 
            if (from === 'view') {
                that.listings.hideListing();
                that.AppState.pop();
                that.AppDrivenStateChange = false;
                return;
            }

            // from #menu 
            if (from === 'menu') {
                that.menu.hideMenu();
                that.AppState.pop();
                that.AppDrivenStateChange = false;
                return;
            }
        });

        this.get('#/explore/:id', function () {

            // if this is the first request, it means they left the app and came back, or bookmarked it
            if (from === 'NONE') {
                var city = that.cities.GetCityById(this.params['id']);

                that.addState({ state: that.States.explore, path: '/' + this.params['id'], data: { keywords: props.$keywords.val() } });

                props.$keywords.val(city.name + ", " + city.country);
                that.cities.hideCities();
                that.search.go(0);
                that.CancelStartup = true;
                //Animations.ShowSplashScreen.play(0);
                
            }
            if (that.AppDrivenStateChange) {
                that.AppDrivenStateChange = false;
                return;
            }

            // #list -> #explore
            if (from === 'list') {
                that.listings.hideListings();
                that.AppState.pop();
                return;
            }
        });

        this.get('#/list', function () {

            if (that.AppDrivenStateChange) {
                that.AppDrivenStateChange = false;
                return;
            }

            // #explore -> #list
            if (from === 'explore') {
                that.listings.showListings();
                that.AppState.push({ state: that.States.list });
                return;
            }

        });

        this.get('#/view', function () {

            if (that.AppDrivenStateChange) {
                that.AppDrivenStateChange = false;
                return;
            }


            that.listings.showListing();
            that.AppState.push({ state: that.States.view });

        });


        this.get('#/menu', function () {

            if (that.AppDrivenStateChange) {
                that.AppDrivenStateChange = false;
                return;
            }
            
            that.menu.showMenu();
            that.AppState.push({ state: that.States.menu });
        });

        

        this.get('', function() {
            // on map load
            if (!that.CancelStartup) {
                google.maps.event.addListenerOnce(that.googlemaps.map, 'idle', function () {
                    // do something only the first time the map is loaded
                    Animations.ShowSplashScreen.play(0);
                });

                that.CancelStartup = true;
            }
        });
    }).run();

    // start up code
    mobile = isMobile();

    // laod the bg img on desktop
    props.$bg.attr('src', SPLASH_IMG);



    // event binding after object is built
    // typeahead plugin
    var typeaheadOptions = {
        matcher: function (item) {
            if (item.toLowerCase().indexOf(this.query.toLowerCase()) !== -1) return true;
            return false;
        },
        showHintOnFocus: true,
        source: that.cities.typeaheadList,
        autoselect: true,
        updater: that.cities.onUpdateTypeahead
    };
    props.$keywords.typeahead(typeaheadOptions);
    props.$keywords2.typeahead(typeaheadOptions);

}



// instantiate the flatbook object
var flatbook = new Flatbook({
    navbar: '#fb-navbar', $navbar: $('#fb-navbar'),
    nav: '#fb-nav', $nav: $('#fb-nav'),
    logo: '#fb-logo', $logo: $('#fb-logo'),
    logo2: '#fb-logo2', $logo2: $('#fb-logo2'),
    menu: '#fb-menu', $menu: $('#fb-menu'),
    mobilebg: '#fb-mobilebg', $mobilebg: $('#fb-mobilebg'),
    citiesFooter: '#fb-citiesFooter', $citiesFooter: $('#fb-citiesFooter'),
    citiesClose: '#fb-close-cities', $citiesClose: $('#fb-close-cities'),
    omni: '#fb-omni', $omni: $('#fb-omni'),
    searchbox: '#fb-search', $searchbox: $('#fb-search'),
    slogan: '#fb-slogan', $slogan: $('#fb-slogan'),
    city: '#fb-city', $city: $('#fb-city'),
    omni2: '#fb-omni2', $omni2: $('#fb-omni2'),
    holder2: '#fb-holder2', $holder2: $('#fb-holder2'),
    error: '#fb-error', $error: $('#fb-error'),
    error2: '#fb-error', $error2: $('#fb-error2'),
    cities: '#fb-cities', $cities: $('#fb-cities'),
    citiesList: '#fb-cities-list', $citiesList: $('#fb-cities-list'),
    citiesTpl: '#fb-cities-list-tpl', $citiesTpl: $('#fb-cities-list-tpl'),
    listings: '#fb-listings', $listings: $('#fb-listings'),
    bg: '#fb-bg', $bg: $('#fb-bg'),
    gmap: '#map-canvas', $gmap: $('#map-canvas'),
    filters: '#fb-filters', $filters: $('#fb-filters'),
    // filters2: '#fb-filters2', $filters2: $('#fb-filters2'),
    // filtersbutton: '#fb-filtersbutton', $filtersbutton: $('#fb-filtersbutton'),
    search: '#fb-search-btn1', $search: $('#fb-search-btn1'),
    search2: '#fb-search-btn2', $search2: $('#fb-search-btn2'),
    keywords: '#fb-keywords', $keywords: $('#fb-keywords'),
    keywords2: '#fb-keywords2', $keywords2: $('#fb-keywords2'),
    body: '#body', $body: $('#body'),
    numresults: '#fb-numresults', $numresults: $('#fb-numresults'),
    resultsbutton: '#fb-resultsbutton', $resultsbutton: $('#fb-resultsbutton'),
    mapbutton: '#fb-mapbutton', $mapbutton: $('#fb-mapbutton'),
    listingbutton: '#fb-listingbutton', $listingbutton: $('#fb-listingbutton'),
    listingsResults: '#fb-listings-results', $listingsResults: $('#fb-listings-results'),
    drawer: '#fb-drawer', $drawer: $('#fb-drawer'),
    drawerbutton: '#fb-drawer-button', $drawerbutton: $('#fb-drawer-button'),
    closeDrawerbutton: '#fb-close-drawer', $closeDrawerbutton: $('#fb-close-drawer'),
    from: '#fb-from', $from: $('#fb-from'),
    to: '#fb-to', $to: $('#fb-to'),
    listingsTpl: '#fb-listings-tpl', $listingsTpl: $('#fb-listings-tpl'),
    listingTpl: '#fb-listing-tpl', $listingTpl: $('#fb-listing-tpl'),
    listingResultTpl: '#fb-listing-result-tpl', $listingResultTpl: $('#fb-listing-result-tpl'),
    infowindowTpl: '#fb-infowindow-tpl', $infowindowTpl: $('#fb-infowindow-tpl'),
    infowindow: '#fb-infowindow', $infowindow: $('#fb-infowindow'),
    cardin: '#fb-card-in', $cardin: $('#fb-card-in'),
    closecard: '#fb-close-card', $closecard: $('#fb-close-card'),
    nothing: '#fb-nothing', $nothing: $('#fb-nothing'),
    filterbutton: '#fb-applyFilters', $filterbutton: $('#fb-applyFilters'),
    filterCheckin: '#fb-ft-checkin', $filterCheckin: $('#fb-ft-checkin'),
    filterCheckout: '#fb-ft-checkout', $filterCheckout: $('#fb-ft-checkout'),
    filterGuests: '#fb-ft-guests', $filterGuests: $('#fb-ft-guests'),
    filterBeds: '#fb-ft-beds', $filterBeds: $('#fb-ft-beds'),
    filterBaths: '#fb-ft-baths', $filterBaths: $('#fb-ft-baths'),
    overlay: '#fb-overlay', $overlay: $('#fb-overlay'),
    listing: '#fb-listing', $listing: $('#fb-listing'),
    listtypebutton: '#fb-list-type', $listtypebutton: $('#fb-list-type'),
    markers: '.overlay', $markers: $('.overlay'),
    selectedListing: '#fb-selected-listing', $selectedListing: $('#fb-selected-listing'),
    listingResult: '#fb-listing-result', $listingResult: $('#fb-listing-result'),
    cardPrev: '#fb-card-prev', $cardPrev: $('#fb-card-prev'),
    cardNext: '#fb-card-next', $cardNext: $('#fb-card-next')
    
});











// utility methods
function isMobile() {
    return verge.viewportW() < MOBILE_BREAK_POINT;
}

$.fn.eqHeights = function (options) {

    var defaults = {
        child: false,
        parentSelector: null
    };
    options = $.extend(defaults, options);

    var el = $(this);
    if (el.length > 0 && !el.data('eqHeights')) {
        $(window).bind('resize.eqHeights', function () {
            el.eqHeights();
        });
        el.data('eqHeights', true);
    }

    var elmnts;
    if (options.child && options.child.length > 0) {
        elmtns = $(options.child, this);
    } else {
        elmtns = $(this).children();
    }

    var prevTop = 0;
    var max_height = 0;
    var elements = [];
    var parentEl;
    elmtns.height('auto').each(function () {

        if (options.parentSelector && parentEl !== $(this).parents(options.parentSelector).get(0)) {
            $(elements).height(max_height);
            max_height = 0;
            prevTop = 0;
            elements = [];
            parentEl = $(this).parents(options.parentSelector).get(0);
        }

        var thisTop = this.offsetTop;

        if (prevTop > 0 && prevTop != thisTop) {
            $(elements).height(max_height);
            max_height = $(this).height();
            elements = [];
        }
        max_height = Math.max(max_height, $(this).height());

        prevTop = this.offsetTop;
        elements.push(this);
    });

    $(elements).height(max_height);
};

// parse date in format mm/dd/yyyy
function parseDate(datestr) {
    var datear = datestr.split('/');
    var date = new Date(datear[2], datear[0] - 1, datear[1], 0, 0, 0, 0);

    return date;
}

// utility function - subtract two arrays
Array.prototype.diff = function (a) {
    return this.filter(function (i) { return !(a.indexOf(i) > -1); });
};