﻿function Menu() {
    var props = {
        navbar: '#fb-navbar', $navbar: $('#fb-navbar'),
        nav: '#fb-nav', $nav: $('#fb-nav'),
        logo: '#fb-logo', $logo: $('#fb-logo'),
        menu: '#fb-menu', $menu: $('#fb-menu'),
        omni: '#fb-omni', $omni: $('#fb-omni'),
        omni2: '#fb-omni2', $omni2: $('#fb-omni2')
    };

    this.inIntro = true;
    var that = this;


    var menuAnimation = {
        play: function() {
            if (that.inIntro) {
                TweenLite.to(props.logo, 0.25, { autoAlpha: 0 });
                TweenLite.set(props.logo, { position: 'absolute' });
                TweenLite.set(props.omni2, { display: 'none' });
            }
            else {
                TweenLite.set(props.omni2, { position: 'absolute' });
                TweenLite.to(props.omni2, 0.25, { autoAlpha: 0, display: 'none' });
            }

            TweenLite.to(props.navbar, 0.25, { backgroundColor: 'rgba(47,145,107,0.3)', height: 'auto' });
            TweenLite.set(props.nav, {display: 'block'});
            TweenLite.to(props.nav, 0.25, { autoAlpha: 1 });
        },
        reverse: function() {
            if (that.inIntro) {
                TweenLite.to(props.logo, 0.25, { autoAlpha: 1 });
                TweenLite.set(props.logo, { position: 'relative' });
            }
            else {
                TweenLite.to(props.omni2, 0.25, { autoAlpha: 1, display: 'block' });
            }

            TweenLite.to(props.navbar, 0.25, { backgroundColor: 'transparent', height: 'auto' });
            TweenLite.to(props.nav, 0.25, { autoAlpha: 0 });
            TweenLite.set(props.nav, {delay: 0.25, display: 'block'});
        }
    };
    // new TimelineLite({
    //     align: 'sequence',
    //     tweens: [
    //         new TweenLite(props.omni2, 0.25, { autoAlpha: '0', overwrite: '1', display: 'none', immediateRender: false }),
    //         new TimelineLite({
    //             tweens: [
    //                 new TweenLite([props.omni, props.logo], 0.25, { overwrite: '1',autoAlpha: '0', display: 'none', immediateRender: false }),
    //                 new TweenLite(props.navbar, 0.25, { delay: 0.25, overwrite: '1',backgroundColor: 'rgba(47,145,107,0.3)', height: 'auto', immediateRender: false }),
    //                 new TweenLite(props.nav, 0.25, { delay: 0.25, overwrite: '1', autoAlpha: '1', display: 'block', immediateRender: false })
    //             ]
    //         })
    //     ]
    // }).pause();

    

    // menu toggle
    var menuShown = false;

    this.hideMenu = function() {
        if (menuShown) {
            // if (flatbook.Animations)
            //     flatbook.Animations.ShowMenu.reverse(0);
            // else
            menuAnimation.reverse(0);

            menuShown = false;
        }
    };

    this.showMenu = function() {
        if (!menuShown) {
            // if (flatbook.Animations)
            //     flatbook.Animations.ShowMenu.play(0, false);
            // else
            menuAnimation.play(0, false);

            menuShown = true;
        }
    };

    this.toggleMenu = function() {
        if (menuShown) {
            that.hideMenu();
        } else {
            that.showMenu();
        }

    };

    // MENU EVENTS - menu button click
    props.$menu.on('click', this.toggleMenu);
}
